
## Rastechng client dashboard package

Laravel dashboard package for rastech client subscribers 


### 1. Integration into Laravel
#### update composer.json
```
 "repositories": [{
        "type": "vcs",
        "url": "https://bitbucket.org/hakeemtunde/rastech-users-dashboard/"
    }]
```

  

```

composer require corebyte/rastech-users-dashboard

```

### 2. User mode

Update the config/auth.php file to use Rastech user model

```

'model' => Corebyte\RastechDashboard\Models\User::class

```

### 3. Customizing users view

Managing different client view pages could be done flexibly by publishing the package views and assets

```
 php artisan vendor:publish --provider="Corebyte\RastechDashboard\AppServiceProvider"
```
  

the command will dump all the views into

> resources/views/vendor/rastechdashboard

  

and the assets(css/js) into

> public/rastechdashboard

  

from here all customization can be done on the views and css/js files **be careful not to delete the rasmap.js file**
