<?php
use Illuminate\Support\Facades\Route;
use Corebyte\RastechDashboard\Http\Controllers\BuyerOrderController;

Route::group(['prefix'=>'buyer'], function() {

    Route::get('orderform', [BuyerOrderController::class, 'form'])->name('buyer.order.form');
    //Orders
    Route::get('orders', [BuyerOrderController::class, 'orders'])->name('buyer.orders.all');
    //Transactions
    Route::get('transactions', [BuyerOrderController::class, 'orders'])->name('buyer.transaction.all');
});

