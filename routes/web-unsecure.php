<?php

use Illuminate\Support\Facades\Route;
use Corebyte\RastechDashboard\Http\Controllers\LoginController;


Route::get('/login', [LoginController::class, 'index'])->name('login');
Route::post('/login', [LoginController::class, 'authenticate'])->name('login');