<?php
use Illuminate\Support\Facades\Route;
use Corebyte\RastechDashboard\Http\Controllers\FarmerUploadController;
use Corebyte\RastechDashboard\Http\Controllers\BuyersController;
use Corebyte\RastechDashboard\Http\Controllers\BuyerOrderController;
use Corebyte\RastechDashboard\Http\Controllers\ServiceProviderController;

Route::group(['prefix'=>'client'], function() { 

    Route::get('farmerrecorduploads', [FarmerUploadController::class, 'geoMappedFarmers'])->name('farmerrecorduploads');
    Route::get('farmerrecordonline', [FarmerUploadController::class, 'farmerEnrolByWeb'])->name('farmerrecordweb');
    Route::get('farmerrecorduploads/{id}', [FarmerUploadController::class, 'show'])->name('farmer.details');
    Route::get('farmerrecord/edit/{id}', [FarmerUploadController::class, 'edit'])->name('farmer.edit');
    Route::get('map/{id}', [FarmerUploadController::class, 'openMap'])->name('farmer.map');

    Route::get('buyers', [BuyersController::class, 'show'])->name('client.buyers');
    Route::delete('buyers/{id}', [BuyersController::class, 'delete'])->name('client.buyers.delete');
    
    Route::get('serviceproviders', [ServiceProviderController::class, 'show'])->name('client.serviceproviders');
    Route::delete('serviceproviders/{id}', [ServiceProviderController::class, 'delete'])->name('client.serviceprovider.delete');
    Route::get('serviceproviders/credentials/{id}', [ServiceProviderController::class, 'generateCredentials'])->name('client.serviceprovider.credential');

    Route::get('orders/{provider_id}', [BuyerOrderController::class, 'getOrdersByProvider'])->name('client.serviceprovider.product.orders');

});


