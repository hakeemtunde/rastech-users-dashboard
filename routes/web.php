<?php

use Illuminate\Support\Facades\Route;
use Corebyte\RastechDashboard\Http\Controllers\LoginController;
use Corebyte\RastechDashboard\Http\Controllers\DashboardController;
use Corebyte\RastechDashboard\Http\Controllers\FarmerUploadController;
use Corebyte\RastechDashboard\Http\Controllers\WebFarmerFormController;

Route::get('/logout', [LoginController::class, 'logout'])->name('logout');

Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');

Route::get('/farmerrecorduploads', [FarmerUploadController::class, 'index'])->name('farmerrecorduploads');

Route::get('farmerform', [WebFarmerFormController::class, 'form'])->name('farmerform.form');
Route::post('farmerform', [WebFarmerFormController::class, 'save'])->name('farmerform.store');
Route::get('farmersform/{famerprofile}', [WebFarmerFormController::class, 'show'])->name('farmerform.edit');
Route::patch('farmersform/{famerprofile}', [WebFarmerFormController::class, 'update'])->name('farmerform.update');
Route::delete('farmersform/{famerprofile}', [WebFarmerFormController::class, 'delete'])->name('farmerform.delete');
Route::get('registeredfarmers', [WebFarmerFormController::class, 'registeredfarmers'] )->name('farmerform.myregfarmers');