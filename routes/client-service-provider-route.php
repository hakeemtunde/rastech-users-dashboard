<?php
use Illuminate\Support\Facades\Route;
use Corebyte\RastechDashboard\Http\Controllers\Provider\ProductController;
use Corebyte\RastechDashboard\Http\Controllers\Provider\MechanisationController;
use Corebyte\RastechDashboard\Http\Controllers\Provider\EquipmentController;
use Corebyte\RastechDashboard\Http\Controllers\BuyerOrderController;

Route::group(['prefix'=>'provider'], function() {

    Route::get('products',[ProductController::class, 'index'])->name('service.provider.products.index');
    Route::get('productsform',[ProductController::class, 'form'])->name('service.provider.products.form');
    Route::get('products/{id}', [ProductController::class, 'edit'])->name('service.provider.products.edit');
    Route::post('products', [ProductController::class, 'store'])->name('service.provider.products.store');
    Route::patch('products/{id}', [ProductController::class, 'update'])->name('service.provider.products.update');
    Route::delete('products/{id}', [ProductController::class, 'delete'])->name('service.provider.products.delete');

    Route::get('mechanization',[MechanisationController::class, 'index'])->name('service.provider.mech.index');
    Route::get('mechanizationform',[MechanisationController::class, 'form'])->name('service.provider.mech.form');
    Route::get('mechanization/{id}', [MechanisationController::class, 'edit'])->name('service.provider.mech.edit');
    Route::post('mechanization', [MechanisationController::class, 'store'])->name('service.provider.mech.store');
    Route::patch('mechanization/{id}', [MechanisationController::class, 'update'])->name('service.provider.mech.update');
    Route::delete('mechanization/{id}', [MechanisationController::class, 'delete'])->name('service.provider.mech.delete');


    // Route::get('clientserviceservice',[ServiceController::class, 'index'])->name('service.provider.services.index');
    // Route::get('clientserviceserviceform',[ServiceController::class, 'form'])->name('service.provider.services.form');
    // Route::get('clientserviceservice/{id}', [ServiceController::class, 'edit'])->name('service.provider.services.edit');
    // Route::post('clientserviceservice', [ServiceController::class, 'store'])->name('service.provider.services.store');
    // Route::patch('clientserviceservice/{id}', [ServiceController::class, 'update'])->name('service.provider.services.update');
    // Route::delete('clientserviceservice/{id}', [ServiceController::class, 'delete'])->name('service.provider.services.delete');

    Route::get('equipments',[EquipmentController::class, 'index'])->name('service.provider.equipments.index');
    Route::get('equipmentsform',[EquipmentController::class, 'form'])->name('service.provider.equipments.form');
    Route::get('equipments/{id}', [EquipmentController::class, 'edit'])->name('service.provider.equipments.edit');
    Route::post('equipments', [EquipmentController::class, 'store'])->name('service.provider.equipments.store');
    Route::patch('equipments/{id}', [EquipmentController::class, 'update'])->name('service.provider.equipments.update');
    Route::delete('equipments/{id}', [EquipmentController::class, 'delete'])->name('service.provider.equipments.delete');

    Route::get('orders', [BuyerOrderController::class, 'getCompanyOrders'])->name('service.provider.product.orders');

});
