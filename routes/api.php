<?php 
use Illuminate\Support\Facades\Route;
use Corebyte\RastechDashboard\Http\Controllers\ApiController\EndPointCallController;
use Corebyte\RastechDashboard\Http\Controllers\ApiController\BuyerOrderController;

Route::get('serviceprovider/productservices/{companyid}/{producttype}', [EndPointCallController::class, 'fetchProductByType']);

Route::post('serviceprovider/buyer/order', [BuyerOrderController::class, 'placeOrder']);