<div class="row">
	<div class="col-md-6 col-xl-4">
		<div class="card mb-3 widget-content bg-midnight-bloom">
			<div class="widget-content-wrapper text-white">
				<div class="widget-content-left">
					<div class="widget-heading">Total</div>
					<!-- 					<div class="widget-subheading">Today</div> -->
				</div>
				<div class="widget-content-right">
					<div class="widget-numbers text-white">
						<span>{{$data->count()}}</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>