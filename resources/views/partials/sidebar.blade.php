<?php use Corebyte\RastechDashboard\Consts\UserRoleConst; ?>
<div class="app-sidebar__inner">
    <ul class="vertical-nav-menu">
        <li><a href="{{route('dashboard')}}" class="mm-active">Dashboard <i class="metismenu-icon pe-7s-rocket"></i>
            </a></li>
            @if($user->userRoles->role_name == UserRoleConst::CLIENT_ROLE )
                @foreach($menus as $client_group_menu)            
                    <li class="app-sidebar__heading">{{$client_group_menu->groupSubMenuItem->group_heading}}</li>
                        <li><a href="#"> 
                            <i class="{{$client_group_menu->groupSubMenuItem->icon }}"></i>
                                {{$client_group_menu->groupSubMenuItem->name}} <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                            </a>
                            <ul>
                            
                            @foreach($client_group_menu->groupSubMenuItem->menuItemGroups as $sub_menu )
                                <li class="mm-active"> 
                                <a href="{{ route($sub_menu->subMenuItem->route) }}" 
                                @if($current_uri == route($sub_menu->subMenuItem->route)) class="mm-active" @endif>  
                                <i class="metismenu-icon"></i> {{ $sub_menu->subMenuItem->name }}
                                </a></li>
                            @endforeach
                            </ul>
                        </li>
                    @endforeach  
                 <!-- remove -->
                 <li class="app-sidebar__heading">Disbursement</li>
        <li class="app-sidebar__heading">Production</li>
        <li class="app-sidebar__heading">Harvesting</li>
        <li class="app-sidebar__heading">Warehouse</li>
        <li class="app-sidebar__heading">Market Linking System</li>
        <li class="app-sidebar__heading">Finance</li>
        <li class="app-sidebar__heading">E-Extension</li>
        <li><a href="#"> <i class="metismenu-icon pe-7s-diamond"></i>
                Resources <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
            </a>
            <ul>
                <li><a href="gallery"> <i class="metismenu-icon"></i> Gallery
                    </a></li>
                <li><a href="#"> <i class="metismenu-icon"></i> Videos
                    </a></li>
            </ul>
        </li>

        <li class="app-sidebar__heading">Important Links</li>
        
        <li class="app-sidebar__heading">Generate QRCoded ID Cards</li>
        <li><a href="#"> <i class="metismenu-icon pe-7s-diamond"></i>
                Generate <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
            </a>
            <ul>
                <li><a href="#"> <i class="metismenu-icon"></i> QRCodes
                    </a></li>
                <li><a href="#"> <i class="metismenu-icon"></i> ID Cards
                    </a></li>
            </ul>
        </li>

                 <!-- remove -->
            @else    
                @foreach($menus as $main_heading => $sub_menu)
                    <li class="app-sidebar__heading">{{$main_heading}}</li>
                        @foreach($sub_menu as $sub_menu_name => $sub_menu_links)
                        <li><a href="#"> 
                            <i class="metismenu-icon pe-7s-diamond"></i>
                                {{$sub_menu_name}} <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                            </a>
                            <ul>
                            @foreach($sub_menu_links as $sub_menu_link_name => $sub_menu_link_path)
                                <li class="mm-active"> 
                                <?php //$sub_menu_link_info = explode('|', $sub_menu_link_path); ?>
                                <a href="{{ $sub_menu_link_path }}" 
                                    @if($current_uri == $sub_menu_link_path) class="mm-active" @endif>  
                                <i class="metismenu-icon"></i> {{ $sub_menu_link_name }}
                                </a></li>
                            @endforeach
                            </ul>
                        </li>
                        @endforeach
                    @endforeach 
            @endif

            @if($user->userRoles->role_name == UserRoleConst::WEB_ENROLLER)
                @include('rastechdashboard::partials.web-farmer-enroller-sidebar')
            @endif
    </ul>

    <br>
    <p>Login as: {{$user->userRoles->role_name}} : {{$user->username}}</p>
</div>

