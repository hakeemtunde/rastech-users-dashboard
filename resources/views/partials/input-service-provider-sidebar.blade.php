<div class="app-sidebar__inner">
    <ul class="vertical-nav-menu">
        <li><a href="dashboard" class="mm-active">Dashboard <i class="metismenu-icon pe-7s-rocket"></i>
            </a></li>
        <li class="app-sidebar__heading">{{ Auth::user()->companyProfile->name }}</li>
        <li><a href="#"> <i class="metismenu-icon pe-7s-diamond"></i>
                Products <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
            </a>
            <ul>
                <li class="mm-active"><a href="products"> <i class="metismenu-icon"></i> 
                Products</a></li>
            </ul>
            
        </li>
        
        <li><a href="#"> <i class="metismenu-icon pe-7s-diamond"></i>
                Mechanisation Service <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
            </a>
            <ul>
            <li><a href="{{ route('mechanisations.create') }}"> <i class="metismenu-icon">
                	</i>Add mechanisation service</a></li>
            <li class="mm-active"><a href="{{ route('mechanisations.index') }}"> <i class="metismenu-icon"></i> 
                View mechanisation services</a></li>
            </ul>
            
        </li>
        
        <li class="app-sidebar__heading">Sales</li>
        <li><a href="#"> <i class="metismenu-icon pe-7s-diamond"></i>
                Farmers Bookings <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
            </a>
            <ul>
                <li><a href="purchasebookings"> <i class="metismenu-icon"></i> Product Bookings
                    </a></li>
                    
                    <li><a href="{{ route('mechanisationbookings.index') }}"> <i class="metismenu-icon"></i> Mechanisation Bookings
                    </a></li>
            </ul>
        </li>
        
        <li class="app-sidebar__heading">Local Market</li>
        <li><a href="#"> <i class="metismenu-icon pe-7s-diamond"></i>
                Item prices <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
            </a>
            <ul>
                <li><a href="{{ route('localmarketprices.create') }}"> 
                	<i class="metismenu-icon"></i> Add item price
                    </a></li>
                    
                    <li><a href="{{ route('localmarketprices.index') }}"> 
                    	<i class="metismenu-icon"></i> View item prices
                    </a></li>
            </ul>
        </li>
        
        
        <li><a href="#"> <i class="metismenu-icon pe-7s-diamond"></i>
                Farmers produce <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
            </a>
            <ul>
                <li><a href="{{ route('localmarketprices.create') }}"> 
                	<i class="metismenu-icon"></i> View proposals
                    </a></li>
                    
            </ul>
        </li>
    </ul>
</div>
