<?php $text ?? 'Delete'; ?>
<form action="{{ route($path, $model) }}" method="post">
{{ csrf_field() }}
    <input type="hidden" value="delete" name="_method">
    <button class="btn btn-danger btn-flat">
        <i class="fa fa-trash"></i>&nbsp;&nbsp; <span>{{$text?? 'Delete'}}</span>
    </button>
</form>
