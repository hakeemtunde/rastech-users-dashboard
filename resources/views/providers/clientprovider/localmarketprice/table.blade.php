<div class="col-md-12">
	<div class="main-card mb-3 card">
		<div class="card-body">
			<table style="width: 100%;" id="example"
				class="table table-hover table-striped table-bordered">
				<thead>
					<tr>
						<th>#</th>
						<th>Name</th>
						<th>Type</th>
						<th>Variety</th>
						<th>Unit/Measure</th>
						<th>Buying price</th>
						<th>Date created</th>
						<th>Last updated date</th>
						<th style="width: 100px;">Action</th>
					</tr>
				</thead>
				<tbody>
				<?php $count=1;?>
					@foreach($local_market_prices as $local_market_price)
						<tr>
							<td>{{$count++}}</td>
    						<td>{{ $local_market_price->name }}</td>
    						<td>{{ $local_market_price->type }}</td>
    						<td>{{ $local_market_price->variety }}</td>
    						<td>{{ $local_market_price->unit }}</td>
    						<td>{{ $local_market_price->price }}</td>
    						<td>{{ $local_market_price->current_date }}</td>
    						<td> {{ $local_market_price->prev_date }}</td>
    						<td>
    							<div class="d-inline-block dropdown">
								<button type="button" data-toggle="dropdown"
									aria-haspopup="true" aria-expanded="false"
									class="btn-shadow dropdown-toggle btn btn-info">
									<span class="btn-icon-wrapper pr-2 opacity-7"> <i
										class="fa fa-business-time fa-w-20"></i>
									</span>
								</button>
								<div tabindex="-1" role="menu" aria-hidden="true"
									class="dropdown-menu dropdown-menu-right">
									<ul class="nav flex-column">
										<li class="nav-item">
    										<a href="{{route('localmarketprices.edit', $local_market_price->id)}}" class="nav-link"> 
    											<i class="fa fa-people"></i> <span>Edit</span>
    										</a>
										</li>
										 
										<li class="nav-item">
											<a href="#" class="nav-link"> 
												<i class="fa fa-people"></i> <span>Delete</span>
											</a>
										</li>
									</ul>
								</div>
							</div>
    						</td>
						</tr>
					@endforeach 
				</tbody>
			</table>
		</div>
	</div>
</div>