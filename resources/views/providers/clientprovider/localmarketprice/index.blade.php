@extends('layout.admin')
@section('page-title')
<div class="page-title-subheading">
	<h3>Local market item prices</h3>
</div>
@endsection
@section('content')

@include('partials.header-counter', ['data'=> $local_market_prices])
<!-- table -->
<div class="row">
	@include('provider.localmarketprice.table')
</div>

@endsection

