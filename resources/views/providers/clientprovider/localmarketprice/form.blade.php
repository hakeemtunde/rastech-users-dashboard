@extends('layout.admin')
@section('page-title')
<div class="page-title-subheading">
@if($local_market_price == null)
	<h3>Local market item price form</h3>
@else
	<h3>Update Local market item price form</h3>
@endif
</div>
@endsection
@section('content')
<div class="row">
    <div class="col-md-6">
        <div class="main-card mb-3 card">
            <div class="card-body">
            @if(Session::has('success'))
            	<div class="alert alert-success fade show" 
            	role="alert">{{ Session::get('success') }}</div>
             @endif   
             
             @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif                
            @if($local_market_price == null)         
                <form action="{{ route('localmarketprices.store') }}" method="post">
                    
            @else
            	<form action="{{ route('localmarketprices.update', $local_market_price->id) }}" method="post">
            	<input type="hidden" value="patch" name="_method">
            @endif
            	{{ csrf_field() }}
                    <div class="position-relative form-group">
                        <label for="" class="">Name</label>
                        <input name="name" 
                        placeholder="Name" type="text" 
                        class="form-control"
                         @if($local_market_price) value="{{ $local_market_price->name }}" @endif>
                    </div>
                    
                    <div class="position-relative form-group">
                        <label for="" class="">Type</label>
                        <input name="type" placeholder="Type" 
                        type="text" class="form-control"
                        @if($local_market_price) value="{{ $local_market_price->type }}" @endif>
                    </div>
                    
                    <div class="position-relative form-group">
                        <label for="" class="">Variety Name (optional)</label>
                        <input name="variety" placeholder="variety" 
                        type="text" class="form-control"
                        @if($local_market_price) value="{{ $local_market_price->variety }}" @endif>
                    </div>
                    
                    <div class="position-relative form-group">
                        <label for="" class="">Unit/Measure</label>
                        <input name="unit" placeholder="Unit" 
                        type="text" class="form-control" 
                       @if($local_market_price) value="{{ $local_market_price->unit }}" @endif>
                    </div>
                    
                    <div class="position-relative form-group">
                        <label for="" class="">Buying price</label>
                        <input name="price" placeholder="price" 
                        type="text" class="form-control" 
                       @if($local_market_price) value="{{ $local_market_price->price }}" @endif>
                    </div>
                    @if($local_market_price == null)
                    <button class="mt-1 btn btn-primary">Add</button>
                    @else
                    	<button class="mt-1 btn btn-primary">Update </button>
                    @endif
                    
                </form>
            </div>
        </div>
    </div>
    
</div>
@endsection