@extends('layout.admin')
@section('page-title')
<div class="page-title-subheading">
	<h3>Purchase Booking</h3>
</div>
@section('content')
<div class="row">
	<div class="col-md-6 col-xl-4">
		<div class="card mb-3 widget-content bg-midnight-bloom">
			<div class="widget-content-wrapper text-white">
				<div class="widget-content-left">
					<div class="widget-heading">Total Bookings</div>
					<!-- 					<div class="widget-subheading">Today</div> -->
				</div>
				<div class="widget-content-right">
					<div class="widget-numbers text-white">
						<span>{{$purchasebookings->count()}}</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



<div class="row">
<div class="col-md-12">
	<div class="main-card mb-3 card">
		<div class="card-body">
			<table style="width: 100%;" id="example"
				class="table table-hover table-striped table-bordered">
				<thead>
					<tr>
						<th>#</th>
						<th>Name</th>
						<th>Phone</th>
						<th>Product Name</th>
						<th>Quantity</th>
						<th>Unit cost</th>
						<th>Total cost</th>
						<th style="width: 100px;">Action</th>
					</tr>
				</thead>
				<tbody>
				<?php $count=1;?>
					@foreach($purchasebookings as $purchasebooking)
						<tr>
							<td>{{$count++}}</td>
    						<td>{{ $purchasebooking->full_name }}</td>
    						<td>{{ $purchasebooking->phone }}</td>
    						<td>{{ $purchasebooking->product->name }}</td>
    						<td>{{ $purchasebooking->quantity }}</td>
    						<td>{{ $purchasebooking->unit_cost }}</td>
    						<td>{{ $purchasebooking->total_cost }}</td>
    						<td>
    							<div class="d-inline-block dropdown">
								<button type="button" data-toggle="dropdown"
									aria-haspopup="true" aria-expanded="false"
									class="btn-shadow dropdown-toggle btn btn-info">
									<span class="btn-icon-wrapper pr-2 opacity-7"> <i
										class="fa fa-business-time fa-w-20"></i>
									</span>
								</button>
								<div tabindex="-1" role="menu" aria-hidden="true"
									class="dropdown-menu dropdown-menu-right">
									<ul class="nav flex-column">
										<li class="nav-item">
    										<a href="" class="nav-link"> 
    											<i class="fa fa-people"></i> <span>Confirm booking</span>
    										</a>
										</li>
										<li class="nav-item">
    										<a href="" class="nav-link"> 
    											<i class="fa fa-people"></i> <span>Confirm payment</span>
    										</a>
										</li>
									</ul>
								</div>
							</div>
    						</td>
						</tr>
					@endforeach 
				</tbody>
			</table>
		</div>
	</div>
</div>
</div>

@endsection

