@extends('rastechdashboard::layout.admin')
@section('page-title')
<div class="page-title-subheading">
	<h3>Mechanisation services</h3>
</div>
@endsection
@section('content')

@include('rastechdashboard::partials.header-counter', ['data'=> $mechanisations])
<!-- table -->
<div class="row">
	@include('rastechdashboard::providers.clientprovider.mechanisations.table')
</div>

@endsection

