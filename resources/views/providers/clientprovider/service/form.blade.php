<?php $mechanisation = null ?>
@extends('rastechdashboard::layout.admin')
@section('page-title')
<div class="page-title-subheading">
@if($mech == null)
	<h3>Mechanisation service form</h3>
@else
	<h3>Update mechanisation service form</h3>
@endif
</div>
@endsection
@section('content')
<div class="row">
    <div class="col-md-6">
        <div class="main-card mb-3 card">
            <div class="card-body">
            @if(Session::has('success'))
            	<div class="alert alert-success fade show" 
            	role="alert">{{ Session::get('success') }}</div>
             @endif   
             
             @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif                
            @if($mech == null)         
                <form action="{{ route('service.provider.mech.store') }}" method="post">
                    
            @else
            	<form action="{{ route('service.provider.mech.update', $mech) }}" method="post">
            	<input type="hidden" value="patch" name="_method">
            @endif
            	{{ csrf_field() }}
                    <div class="position-relative form-group">
                        <label for="" class="">Name</label>
                        <input name="name" 
                        placeholder="Name" type="text" 
                        class="form-control"
                         @if($mech) value="{{ $mech->name }}" @endif>
                    </div>
                    
                    <div class="position-relative form-group">
                        <label for="" class="">Measure/unit</label>
                        <input name="unit" placeholder="Unit" 
                        type="text" class="form-control"
                        @if($mech) value="{{ $mech->unit }}" @endif
                        >
                    </div>
                    
                    <div class="position-relative form-group">
                        <label for="" class="">Cost per unit/measure</label>
                        <input name="cost" placeholder="Unit price" 
                        type="text" class="form-control" 
                        @if($mech) value="{{ $mech->cost }}" @endif>
                    </div>
                    @if($mech == null)
                    <button class="mt-1 btn btn-primary">Add service</button>
                    @else
                    	<button class="mt-1 btn btn-primary">Update service</button>
                    @endif
                    
                </form>
            </div>
        </div>
    </div>
    
</div>
@endsection