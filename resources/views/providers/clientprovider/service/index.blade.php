@extends('rastechdashboard::layout.admin')
@section('page-title')
<div class="page-title-subheading">
	<h3>Services</h3>
</div>
@endsection
@section('content')

@include('rastechdashboard::partials.header-counter', ['data'=> $services])
<!-- table -->
<div class="row">
	@include('rastechdashboard::providers.clientprovider.service.table')
</div>

@endsection

