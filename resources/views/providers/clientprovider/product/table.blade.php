<div class="col-md-12">
	<div class="main-card mb-3 card">
		<div class="card-body">
			<table style="width: 100%;" id="example"
				class="table table-hover table-striped table-bordered">
				<thead>
					<tr>
						<th>#</th>
						<th>Type</th>
						<th>Name</th>
						<th>Variety</th>
						<th>Unit</th>
						<th>Unit cost</th>
						<th>Locations</th>
						<th>Action</th>
						<th>Delete</th>
					</tr>
				</thead>
				<tbody>
				<?php $count=1;?>
					@foreach($products as $product)
						<tr>
							<td>{{$count++}}</td>
    						<td>{{ $product->type }}</td>
    						<td>{{ $product->name }}</td>
    						<td>{{ $product->variety }}</td>
    						<td>{{ $product->measure_unit }}</td>
    						<td>{{ $product->unit_price }}</td>
    						<td>{{ $product->locations }}</td>
    						<td>
    							<div class="d-inline-block dropdown">
								<button type="button" data-toggle="dropdown"
									aria-haspopup="true" aria-expanded="false"
									class="btn-shadow dropdown-toggle btn btn-info">
									<span class="btn-icon-wrapper pr-2 opacity-7"> <i
										class="fa fa-business-time fa-w-20"></i>
									</span>
								</button>
								<div tabindex="-1" role="menu" aria-hidden="true"
									class="dropdown-menu dropdown-menu-right">
									<ul class="nav flex-column">
										<li class="nav-item">
    										<a href="{{route('service.provider.products.edit', $product)}}" class="nav-link">
    											<i class="fa fa-people"></i> <span>Edit</span>
    										</a>
										</li>
									</ul>
								</div>
							</div>
    						</td>
							<td>
                            @include('rastechdashboard::partials.delete',
									['path'=> 'service.provider.products.delete',
									'model'=> $product])
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
