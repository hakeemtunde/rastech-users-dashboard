@extends('rastechdashboard::layout.admin') 
@section('page-title')
<div class="page-title-subheading">
	<h3>{{$provider->name??''}} Orders</h3>
</div>
@endsection @section('content')
<div class="row">
    <div class="col-md-12">
            <div class="main-card mb-3 card">
                <div class="card-body">
                <table id="example" class="table table-hover table-striped table-bordered">
    				<thead>
    					<tr>
    						<th>S</th>
    						<th>Order Id</th>
    						<th>Buyer Name</th>
							<th>Phone</th>
							<th>email</th>
                            <th>Product</th>
    						<th>Qty</th>
                            <th>Total</th>
                            <th>Order date</th>
							<th>Action</th>
    						
    					</tr>
    				</thead>
    				<tbody>
    				<?php $couter = 0; ?>
    					@foreach($orders as $order)
    					<?php $couter++; ?>
    					<tr>
    						<td scope="row">{{ $couter }}</td>
                            <td>{{ $order->order_id }}</td>
    						<td>{{ $order->clientBuyer->getFullName() }}</td>
							<td>{{ $order->clientBuyer->phoneNumber }}</td>
							<td>{{ $order->clientBuyer->email }}</td>
    						<td>{{ $order->product->name }}</td>
                            <td>{{ $order->quantity }}</td>
                            <td>{{ $order->total }}</td>
    						<td>{{ $order->created_at->format('d-m-Y') }}</td>
							<td>
    							<div class="d-inline-block dropdown">
								<button type="button" data-toggle="dropdown"
									aria-haspopup="true" aria-expanded="false"
									class="btn-shadow dropdown-toggle btn btn-info">
									<span class="btn-icon-wrapper pr-2 opacity-7"> <i
										class="fa fa-business-time fa-w-20"></i>
									</span>
								</button>
								<div tabindex="-1" role="menu" aria-hidden="true"
									class="dropdown-menu dropdown-menu-right">
									<ul class="nav flex-column">
										<li class="nav-item">
    										<a href="" class="nav-link">
    											<i class="fa fa-people"></i> <span>View profile</span>
    										</a>
										</li>
										<li class="nav-item">
    										<a href="" class="nav-link">
    											<i class="fa fa-gear"></i> <span>Update order</span>
    										</a>
										</li>
									</ul>
								</div>
							</div>
    						</td>							
    					</tr>
    					@endforeach
    
    			</table>
                </div>
            </div>
        </div>
</div>
@endsection