@extends('rastechdashboard::layout.admin')
@section('page-title')
<div class="page-title-subheading">
	<h3>Products</h3>
</div>
@endsection 
@section('content')
<div class="row">
    <div class="col-md-6">
        <div class="main-card mb-3 card">
            <div class="card-body">
            @if(Session::has('success'))
            	<div class="alert alert-success fade show" role="alert">Product added successfully</div>
             @endif   
             
             @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif                         
                <h5 class="card-title">Product form</h5>
                <form action="{{ route('service.provider.products.store') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="position-relative form-group">
                        <label for="" class="">Type</label>
                        <select name="type" class="form-control">
                        <option value="">None</option>
                            <option>SEED</option>
                            <option>FERTILIZER</option>
                            <option>AGRO-CHEMICAL</option>
                        </select>
                    </div>
                    <div class="position-relative form-group">
                        <label for="" class="">Name</label>
                        <input name="name" placeholder="Name" type="text" class="form-control">
                    </div>
                    
                    <div class="position-relative form-group">
                        <label for="" class="">Variety</label>
                        <input name="variety" placeholder="Variety" type="text" class="form-control">
                    </div>
                    
                    <div class="position-relative form-group">
                        <label for="" class="">Measure/unit</label>
                        <input name="measure_unit" placeholder="Unit" type="text" class="form-control">
                    </div>
                    
                    <div class="position-relative form-group">
                        <label for="" class="">unit price</label>
                        <input name="unit_price" placeholder="Unit price" type="text" class="form-control">
                    </div>
                    
                    <div class="position-relative form-group">
                        <label for="exampleText" class="">Pickup locations</label>
                        <textarea name="locations" id="exampleText" class="form-control"></textarea>
                    </div>
                    
                    <button class="mt-1 btn btn-primary">Add product</button>
                </form>
            </div>
        </div>
    </div>
    
</div>



@endsection

