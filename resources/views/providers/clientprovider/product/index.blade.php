@extends('rastechdashboard::layout.admin')
@section('page-title')
<div class="page-title-subheading">
	<h3>Products</h3>
</div>
@endsection
@section('content')

@include('rastechdashboard::partials.header-counter', ['data'=> $products])
<!-- table -->
<div class="row">
	@include('rastechdashboard::providers.clientprovider.product.table')
</div>

@endsection

