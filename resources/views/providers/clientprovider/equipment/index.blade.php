@extends('rastechdashboard::layout.admin')
@section('page-title')
<div class="page-title-subheading">
	<h3>Equipments</h3>
</div>
@endsection
@section('content')

@include('rastechdashboard::partials.header-counter', ['data'=> $equipments])
<!-- table -->
<div class="row">
	@include('rastechdashboard::providers.clientprovider.equipment.table')
</div>

@endsection

