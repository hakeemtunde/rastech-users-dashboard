@extends('layout.admin')
@section('page-title')
<div class="page-title-subheading">
	<h3>Mechanisation Bookings</h3>
</div>
@endsection
@section('content')
@include('partials.header-counter', ['data'=>$mechanisationBookings])

<div class="col-md-12">
	<div class="main-card mb-3 card">
		<div class="card-body">
			<table style="width: 100%;" id="example"
				class="table table-hover table-striped table-bordered">
				<thead>
					<tr>
						<th>#</th>
						<th>Name</th>
						<th>Measure/unit</th>
						<th>Price/measure </th>
						<th>Land size</th>
						<th>Full Name</th>
						<th>Phone</th>
						<th>Date</th>
						<th style="width: 100px;">Action</th>
					</tr>
				</thead>
				<tbody>
				<?php $count=1;?>
					@foreach($mechanisationBookings as $booking)
						<tr>
							<td>{{$count++}}</td>
    						<td>{{ $booking->mechanisation->name }}</td>
    						<td>{{ $booking->mechanisation->unit }}</td>
    						<td>{{ $booking->unit_cost }}</td>
    						<td>{{ $booking->land_size }}</td>
    						<td>{{ $booking->full_name }}</td>
    						<td>{{ $booking->phone }}</td>
    						<td>{{ $booking->created_at->format('d/m/Y/ H:i:s a')}}</td>
    						
    						<td>
    							<div class="d-inline-block dropdown">
								<button type="button" data-toggle="dropdown"
									aria-haspopup="true" aria-expanded="false"
									class="btn-shadow dropdown-toggle btn btn-info">
									<span class="btn-icon-wrapper pr-2 opacity-7"> <i
										class="fa fa-business-time fa-w-20"></i>
									</span>
								</button>
								<div tabindex="-1" role="menu" aria-hidden="true"
									class="dropdown-menu dropdown-menu-right">
									<ul class="nav flex-column">
										<li class="nav-item">
    										<a href="{{route('mechanisationbookings.show', $booking->id)}}" class="nav-link"> 
    											<i class="fa fa-people"></i> <span>Confirm booking</span>
    										</a>
										</li>
										<li class="nav-item">
    										<a href="#" class="nav-link"> 
    											<i class="fa fa-people"></i> <span>Map</span>
    										</a>
										</li>
										<li class="nav-item">
    										<a href="{{route('mechanisationbookings.destroy', $booking->id)}}" class="nav-link"> 
    											<i class="fa fa-people"></i> <span>Delete</span>
    										</a>
										</li>
									</ul>
								</div>
							</div>
    						</td>
						</tr>
					@endforeach 
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection

