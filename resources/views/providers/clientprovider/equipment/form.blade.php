<?php $equipmentanisation = null ?>
@extends('rastechdashboard::layout.admin')
@section('page-title')
<div class="page-title-subheading">
@if($equipment == null)
	<h3>Equipment form</h3>
@else
	<h3>Update Equipment form</h3>
@endif
</div>
@endsection
@section('content')
<div class="row">
    <div class="col-md-6">
        <div class="main-card mb-3 card">
            <div class="card-body">
            @if(Session::has('success'))
            	<div class="alert alert-success fade show" 
            	role="alert">{{ Session::get('success') }}</div>
             @endif   
             
             @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif                
            @if($equipment == null)         
                <form action="{{ route('service.provider.equipments.store') }}" method="post">
                    
            @else
            	<form action="{{ route('service.provider.equipments.update', $equipment) }}" method="post">
            	<input type="hidden" value="patch" name="_method">
            @endif
            	{{ csrf_field() }}
                    <div class="position-relative form-group">
                        <label for="" class="">Name</label>
                        <input name="name" 
                        placeholder="Name" type="text" 
                        class="form-control"
                         @if($equipment) value="{{ $equipment->name }}" @endif>
                    </div>
                    
                    <div class="position-relative form-group">
                        <label for="" class="">Type</label>
                        <input name="type" placeholder="Type" 
                        type="text" class="form-control"
                        @if($equipment) value="{{ $equipment->type }}" @endif
                        >
                    </div>

                    <div class="position-relative form-group">
                        <label for="" class="">Model</label>
                        <input name="model" placeholder="Model" 
                        type="text" class="form-control"
                        @if($equipment) value="{{ $equipment->model }}" @endif
                        >
                    </div>

                    <div class="position-relative form-group">
                        <label for="" class="">Capacity</label>
                        <input name="capacity" placeholder="Capacity" 
                        type="text" class="form-control"
                        @if($equipment) value="{{ $equipment->capacity }}" @endif
                        >
                    </div>


                    <div class="position-relative form-group">
                        <label for="" class="">Description</label>
                        <textarea name="description" 
                            placeholder="Description" class="form-control">@if($equipment) {{ $equipment->description }} @endif</textarea>
                    </div>
                    
                    <div class="position-relative form-group">
                        <label for="" class="">Cost</label>
                        <input name="cost" placeholder="Unit price" 
                        type="text" class="form-control" 
                        @if($equipment) value="{{ $equipment->cost }}" @endif>
                    </div>
                    @if($equipment == null)
                    <button class="mt-1 btn btn-primary pull-right">Add</button>
                    @else
                    	<button class="mt-1 btn btn-primary pull-right">Update</button>
                    @endif
                    
                </form>
            </div>
        </div>
    </div>
    
</div>
@endsection