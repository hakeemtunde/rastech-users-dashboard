@extends('rastechdashboard::layout.admin') @section('page-title')
<div class="page-title-subheading">
	<h3>Service providers</h3>
</div>
@endsection
@section('content')
<div class="row">
	<div class="col-md-6 col-xl-4">
		<div class="card mb-3 widget-content bg-midnight-bloom">
			<div class="widget-content-wrapper text-white">
				<div class="widget-content-left">
					<div class="widget-heading">Total records</div>
				</div>
				<div class="widget-content-right">
					<div class="widget-numbers text-white">
						<span>{{ $providers->count() }}</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12 col-xl-12">
    	<div class="main-card mb-3 card">
    		<div class="card-body">
    			<table id="example" class="table table-hover table-striped table-bordered">
    				<thead>
    					<tr>
    						<th>S</th>
    						<th>Company name</th>
    						<th>Phone</th>
                            <th>email</th>
							<th>Address</th>
    						<th>Date</th>
							<th>Gen. credential</th>
    						<th>Delete</th>
							<th>Actions </th>
    					</tr>
    				</thead>
    				<tbody>
    				<?php $couter = 0; ?>
    					@foreach($providers as $provider)
    					<?php $couter++; ?>
    					<tr>
    						<td scope="row">{{ $couter }}</td>
    						<td>{{ $provider->name }}</td>
    						<td>{{ $provider->phoneNumber }}</td>
                            <td>{{ $provider->email }}</td>
							<td>{{ $provider->address }}</td>
    						<td>{{ $provider->created_at->format('m-d-Y') }}</td>
							<td>
    							<a class="btn btn-success"
								href="{{ route('client.serviceprovider.credential', $provider) }}">
									Generate
								</a>
    						</td>
    						<td>
							@include('rastechdashboard::partials.delete',
									['path'=> 'client.serviceprovider.delete',
									'model'=> $provider])
    						</td>
							<td>
							<div class="d-inline-block dropdown">
								<button type="button" data-toggle="dropdown"
									aria-haspopup="true" aria-expanded="false"
									class="btn-shadow dropdown-toggle btn btn-info">
									<span class="btn-icon-wrapper pr-2 opacity-7"> <i
										class="fa fa-business-time fa-w-20"></i>
									</span>
								</button>
								<div tabindex="-1" role="menu" aria-hidden="true"
									class="dropdown-menu dropdown-menu-right">
									<ul class="nav flex-column">
										<li class="nav-item">
    										<a href="{{route('client.serviceprovider.product.orders', $provider)}}" class="nav-link">
    											<i class="fa fa-people"></i> <span>View orders</span>
    										</a>
										</li>
									</ul>
								</div>
							</div>
							</td>
    					</tr>
    					@endforeach
    
    			</table>
    		</div>
    	</div>
    </div>
</div>
@endsection
