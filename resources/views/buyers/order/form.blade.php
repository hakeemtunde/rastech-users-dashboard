@extends('rastechdashboard::layout.admin') 
@section('page-title')
<div class="page-title-subheading">
	<h3>Order form</h3>
</div>
@endsection @section('content')
<div class="row">
<div class="col-md-12">
        <div class="main-card mb-3 card">
            <div class="card-body">
            @if(Session::has('success'))
            	<div class="alert alert-success fade show" role="alert">{{Session::get('success') }}</div>
             @endif   
             
             @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form action="" method="post">
            {{ csrf_field() }}
            <input type="hidden" id="client-id" value="{{$client_id}}" name="client_id" >
            <input type="hidden" id="buyer-id" value="{{$user_id}}" name="buyer_id" >

            <div id="smartwizard">
                <ul class="forms-wizard">
                    <li>
                        <a href="#step-1">
                            <em>1</em><span>Company products & services</span>
                        </a>
                    </li>
                    <li>
                        <a href="#step-2">
                            <em>2</em><span>Product detail</span>
                        </a>
                    </li>
                    <li>
                        <a href="#step-3">
                            <em>3</em><span>Order summary</span>
                        </a>
                    </li>
                    <li>
                        <a href="#step-4">
                            <em>4</em><span>Finish</span>
                        </a>
                    </li>
                </ul>
                <div class="form-wizard-content">
                    <div id="step-1">
                        <div class="position-relative form-group">
                            <label for="" class="">Company: </label>
                                <select id="companies" name="company" class="form-control">
                                    <option value="0"> -- Select company-- </option>
                                    @foreach($client_service_providers as $service_provider)
                                        <option value="{{$service_provider->id}}-{{$service_provider->name}}"> {{$service_provider->name}} </option>
                                    @endforeach
                                </select>
                        </div>
                        <div class="position-relative form-group">
                            <label for="" class="">Select product type: </label>
                            <select id="type" name="product-type" class="form-control">
                                <option value="0"> -- Select product-- </option>
                                <option value="products"> Products </option>
                                <option value="services"> Services </option>
                            </select>
                        </div>
                    </div>  <!-- step-1 -->
                    <div id="step-2">
                        <div class="position-relative form-group">
                            <label for="" class="">Products/Services: </label>
                                <select id="products" name="products" class="form-control">
                                    <option value=""> -- Select Products-- </option>
                                </select>
                        </div>
                        <div class="position-relative form-group">
                            <label for="" class="">Unit price </label>
                            <input id="price" name="price" disabled type="text" class="form-control">
                        </div>
                        <div class="position-relative form-group">
                            <label for="" class="">Quantity </label>
                            <input id="quantity" name="quantity" type="text" class="form-control">
                        </div>
                    </div> <!-- step-2 -->
                    <div id="step-3">
                        <table class="table table-borderless">
                            <tr>
                                <td style="width:100px" class="text-mute">Company Name: </td>
                                <td colspan="2">
                                    <h3 id="company-name"></h3>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:100px">Type: </td>
                                <td style="width:400px">
                                    <h3 id="product-type"></h3>
                                </td>
                                <td style="width:100px">Name: </td>
                                <td>
                                    <h3 id="product-name"></h3>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:100px">Unit price: </td>
                                <td>
                                    <h3 id="unit-price"></h3>
                                </td>
                                <td style="width:100px">Quantity: </td>
                                <td><h3 id="product-quantity"></h3></td>
                            </tr>
                            
                            <tr>
                                <td></td>
                                <td></td>
                                <td style="width:100px">Total: </td>
                                <td>
                                    <h3 id="total-price" class="text-success"></h3>
                                </td>
                            </tr>
                        </table>
                    </div><!-- step-3 -->
                    <div id="step-4">
                        <div class="no-results">
                            <div class="swal2-icon swal2-success swal2-animate-success-icon">
                                <div class="swal2-success-circular-line-left" style="background-color: rgb(255, 255, 255);"></div>
                                <span class="swal2-success-line-tip"></span>
                                <span class="swal2-success-line-long"></span>
                                <div class="swal2-success-ring"></div>
                                <div class="swal2-success-fix" style="background-color: rgb(255, 255, 255);"></div>
                                <div class="swal2-success-circular-line-right" style="background-color: rgb(255, 255, 255);"></div>
                            </div>
                            <div class="results-subtitle mt-4">Finished!</div>
                            <div class="results-title">Order placed successfully</div>
                            <div class="results-title text-success" id="order-id"></div>
                            <div class="mt-3 mb-3"></div>
                        </div>
                    </div>
                </div>  <!-- form-wizard-content -->
            </div>  
            <div class="clearfix">
                <button type="button" id="placeorder-btn" class="btn-shadow btn-wide float-right btn-pill btn-hover-shine btn btn-success">Place order</button>
                <button type="button" id="next-btn" class="btn-shadow btn-wide float-right btn-pill btn-hover-shine btn btn-primary">Next</button>
                <button type="button" id="prev-btn" class="btn-shadow float-right btn-wide btn-pill mr-3 btn btn-outline-secondary">Previous</button>
            </div>
                   
            </form>
            </div>
        </div>
    </div>
@endsection    
@push('scripts')
<script src="{{asset('rastechdashboard/js/resource.js')}}" type="text/javascript"></script>
@endpush
