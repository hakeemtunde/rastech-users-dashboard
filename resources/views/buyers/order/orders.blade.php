@extends('rastechdashboard::layout.admin') 
@section('page-title')
<div class="page-title-subheading">
	<h3>Orders</h3>
</div>
@endsection @section('content')
<div class="row">
    <div class="col-md-12">
            <div class="main-card mb-3 card">
                <div class="card-body">
                <table id="example" class="table table-hover table-striped table-bordered">
    				<thead>
    					<tr>
    						<th>S</th>
    						<th>Order Id</th>
    						<th>Service provider</th>
                            <th>Product</th>
    						<th>Qty</th>
                            <th>Unit price</th>
                            <th>Total</th>
                            <th>Date</th>
    						<th>Status</th>
    					</tr>
    				</thead>
    				<tbody>
    				<?php $couter = 0; ?>
    					@foreach($orders as $order)
    					<?php $couter++; ?>
    					<tr>
    						<td scope="row">{{ $couter }}</td>
                            <td>{{ $order->order_id }}</td>
    						<td>{{ $order->serviceProvider->name }}</td>
    						<td>{{ $order->product->name }}</td>
                            <td>{{ $order->quantity }}</td>
                            <td>{{ $order->price }}</td>
                            <td>{{ $order->total }}</td>
    						<td>{{ $order->created_at->format('m-d-Y') }}</td>
                            <td class="text-success">In progress</td>
    					</tr>
    					@endforeach
    
    			</table>
                </div>
            </div>
        </div>
</div>
@endsection