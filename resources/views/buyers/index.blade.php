@extends('rastechdashboard::layout.admin') @section('page-title')
<div class="page-title-subheading">
	<h3>Buyers</h3>
</div>
@endsection
@section('content')
<div class="row">
	<div class="col-md-6 col-xl-4">
		<div class="card mb-3 widget-content bg-midnight-bloom">
			<div class="widget-content-wrapper text-white">
				<div class="widget-content-left">
					<div class="widget-heading">Total records</div>
				</div>
				<div class="widget-content-right">
					<div class="widget-numbers text-white">
						<span>{{ $buyers->count() }}</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12 col-xl-12">
    	<div class="main-card mb-3 card">
    		<div class="card-body">
    			<table id="example" class="table table-hover table-striped table-bordered">
    				<thead>
    					<tr>
    						<th>S</th>
    						<th>First Name</th>
    						<th>Middle Name</th>
    						<th>Surename</th>
    						<th>Phone</th>
                            <th>email</th>
    						<th>Date</th>
    						<th>Delete</th>
    
    					</tr>
    				</thead>
    				<tbody>
    				<?php $couter = 0; ?>
    					@foreach($buyers as $buyer)
    					<?php $couter++; ?>
    					<tr>
    						<td scope="row">{{ $couter }}</td>
    						<td>{{ $buyer->firstName }}</td>
    						<td>{{ $buyer->middleName }}</td>
    						<td>{{ $buyer->sureName }}</td>
    						<td>{{ $buyer->phoneNumber }}</td>
                            <td>{{ $buyer->email }}</td>
    						<td>{{ $buyer->created_at->format('d-m-Y') }}</td>
    						<td>
    							<form action="{{ route('client.buyers.delete', $buyer) }}" method="post">
    								{{ csrf_field() }}
									<input type="hidden" value="delete" name="_method">
									<button class="btn btn-danger">Delete</button>
								</form>
    						</td>
    					</tr>
    					@endforeach
    
    			</table>
    		</div>
    	</div>
    </div>
</div>
@endsection
