<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Language" content="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Farm Management System - Analytics Dashboard</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
<meta name="description"
	content="This is an example dashboard created using build-in elements and components.">
<meta name="msapplication-tap-highlight" content="no">

<link href="{{ asset('rastechdashboard/css/main.d810cf0ae7f39f28f336.css') }}" rel="stylesheet">
<!-- pnotify  -->
<link href="{{ asset('rastechdashboard/css/pnotify.custom.min.css') }}" media="all" rel="stylesheet" type="text/css" />
@yield('css')
</head>

<body>
	<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
		<div class="app-header header-shadow">
            @include('rastechdashboard::partials.header-nav')
		</div>
		<!-- ./header -->
		<div class="app-main">
			<div class="app-sidebar sidebar-shadow">
				<div class="app-header__logo">
					<div class="logo-src"></div>
					<div class="header__pane ml-auto">
						<div>
							<button type="button"
								class="hamburger close-sidebar-btn hamburger--elastic"
								data-class="closed-sidebar">
								<span class="hamburger-box"> <span class="hamburger-inner"></span>
								</span>
							</button>
						</div>
					</div>
				</div>
				<div class="app-header__mobile-menu">
					<div>
						<button type="button"
							class="hamburger hamburger--elastic mobile-toggle-nav">
							<span class="hamburger-box"> <span class="hamburger-inner"></span>
							</span>
						</button>
					</div>
				</div>
				<div class="app-header__menu">
					<span>
						<button type="button"
							class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
							<span class="btn-icon-wrapper"> <i
								class="fa fa-ellipsis-v fa-w-6"></i>
							</span>
						</button>
					</span>
				</div>
				<div class="scrollbar-sidebar">
					@include('rastechdashboard::partials.sidebar')
				</div>
			</div>
			<!-- ./siderbar -->
			<div class="app-main__outer">
				<div class="app-main__inner">
					<div class="app-page-title">
						<div class="page-title-wrapper">
							<div class="page-title-heading">
								<div class="page-title-icon">
									<i class="pe-7s-medal icon-gradient bg-tempting-azure"></i>
								</div>
								<div>@yield('page-title')</div>
							</div>
							<div class="page-title-actions">@yield('page-action')</div>
						</div>
					</div>
					@yield('content')
				</div>
			</div>
		</div>

        </div>

		<script type="text/javascript"	src="{{ asset('rastechdashboard/js/main.d810cf0ae7f39f28f336.js') }}"></script>
		<script src="{{ asset('rastechdashboard/js/jquery-2.2.3.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('rastechdashboard/js/pnotify.custom.min.js') }}"></script>
		
		<script type="text/javascript">
		@if( Session::has('success') )
				new PNotify({
					title: 'Success',
					text: '{{ Session::get('success') }}',
					type: 'success'
				});
		@endif
		@if( Session::has('error') )
			new PNotify({
				title: 'Error',
				text: '{{ Session::get('error') }}',
				type: 'error'
			});
		@endif
		</script>
		@stack('scripts')
</body>

</html>
