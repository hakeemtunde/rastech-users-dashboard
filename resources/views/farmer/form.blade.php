<?php use Corebyte\RastechDashboard\Util\StorageUtil; ?>
@extends('rastechdashboard::layout.admin')
@section('page-title')
<div class="page-title-subheading">
<h3>Farmer's form</h3>
</div>
@endsection
@section('content')
<div class="row">
    <div class="col-md-6">
        <div class="main-card mb-3 card">
            <div class="card-body">
            @if(Session::has('success'))
            	<div class="alert alert-success fade show" 
            	role="alert">{{ Session::get('success') }}</div>
             @endif   
             
             @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif          
            
            @if ($farmerprofile != null)
            	<form action="{{route('farmerform.update', $farmerprofile) }}" method="post" 
                enctype="multipart/form-data">
                <input type="hidden" value="patch" name="_method">
            @else
            	<form action="{{route('farmerform') }}" method="post" 
                enctype="multipart/form-data">
            @endif      
                     
            	{{ csrf_field() }}
            	<div class="position-relative form-group">
                        <label for="" class="">BVN No</label>
                        <input name="bvnNumber" 
                        placeholder="BVN No" type="text" 
                        class="form-control"
                        @if ($farmerprofile) value="{{ $farmerprofile->bvnNumber }}"
                        @else value="{{old('bvnNumber') }}"
                        @endif
                        >
                    </div>
                    <div class="position-relative form-group">
                        <label for="" class="">First Name</label>
                        <input name="firstName" 
                        placeholder="First Name" type="text" 
                        class="form-control"
                        @if ($farmerprofile) value="{{ $farmerprofile->firstName }}"
                        @else value="{{old('firstName') }}"
                        @endif 
                        >
                    </div>
                    
                    <div class="position-relative form-group">
                        <label for="" class="">Middle Name</label>
                        <input name="middleName" 
                        placeholder="Middle Name" type="text" 
                        class="form-control" 
                        @if ($farmerprofile) value="{{ $farmerprofile->middleName }}"
                        @else value="{{old('middleName') }}"
                        @endif 
                        >
                    </div>
                    
                    <div class="position-relative form-group">
                        <label for="" class="">Last Name</label>
                        <input name="sureName" 
                        placeholder="Last Name" type="text" 
                        class="form-control" 
                        @if ($farmerprofile) value="{{ $farmerprofile->sureName }}"
                        @else value="{{old('sureName') }}" 
                        @endif>
                    </div>
                    
                    <div class="position-relative form-group">
                        <label for="" class="">Address</label>
                        <input name="homeAddress"  
                        type="textarea" class="form-control" 
                        @if ($farmerprofile) value="{{ $farmerprofile->homeAddress }}"
                        @else value="{{old('homeAddress') }}" 
                        @endif>
                    </div>
                    
                    <div class="position-relative form-group">
                        <label for="" class="">Phone No</label>
                        <input name="phoneNumber" 
                        placeholder="Phone No" type="text" 
                        class="form-control" 
                        @if ($farmerprofile) value="{{ $farmerprofile->phoneNumber }}"
                        @else value="{{old('phoneNumber') }}" 
                        @endif>
                    </div>
                    
                    <div class="position-relative form-group">
                        <label for="" class="">Gender: </label>
                        
                        <?php $gender = $farmerprofile ? $farmerprofile->gender : old('gender'); ?>
                        Male <input name="gender" value="Male" 
                        type="radio" @if($gender=='Male') checked @endif>
                        Female <input name="gender" value="Female" 
                         type="radio" @if($gender=='Female') checked @endif >
                    </div>
                    
                    <div class="position-relative form-group">
                        <label for="" class="">State</label>
                        <input name="state" placeholder="State" 
                        type="text" class="form-control" 
                        @if ($farmerprofile) value="{{ $farmerprofile->state }}"
                        @else value="{{old('state') }}" 
                        @endif>  
                    </div>
                         
                     <div class="position-relative form-group">
                        <label for="" class="">LGA</label>
                        <input name="lga" placeholder="LGA" 
                        type="text" class="form-control" 
                        @if ($farmerprofile) value="{{ $farmerprofile->lga }}"
                        @else value="{{old('lga') }}" 
                        @endif>  </div>
                    
                    
              
            </div>
        </div>
    </div>
    
    <div class="col-md-6">
        <div class="main-card mb-3 card">
            <div class="card-body">
            @if($farmerprofile != null && $farmerprofile->imageName != null)
                <?php 
                $img_url = StorageUtil::getImageLocation($farmerprofile, 'imageName');
                ?>
                <img alt="" src="{{$img_url}}"	width="110px" height="120px">
            @endif
                    <div class="position-relative form-group">
                        <label for="" class="">Picture: </label>
                        <input name="picture"  
                        type="file" class="form-control">
                    </div>
                    
                    <div class="position-relative form-group">
                        <label for="" class="">Identification: </label>
                        <input name="identification"  
                        type="file" class="form-control" 
                        value="{{old('identification') }}">
                    </div>
      
                    <div class="position-relative form-group">
                        <label for="" class="">ID type:</label>
                        <select class="form-group" name="idType">
                        	<option>National ID</option>
                        	<option>Voters Card</option>
                        	<option>Driving License</option>
                        	<option>International Passport</option>
                        </select>  </div>
                        
                        <div class="position-relative form-group">
                        <label for="" class="">ID No:</label>
                        <input name="idNumber" placeholder="ID No" 
                        type="text" class="form-control" 
                        value="{{old('idNumber') }}" >  </div>
                    
                    <div class="position-relative form-group">
                        <label for="" class="">Document 1: </label>
                        <input name="document1Name"  
                        type="file" class="form-control">
                    </div>
                    <div class="position-relative form-group">
                        <label for="" class="">Document 2: </label>
                        <input name="document2Name"  
                        type="file" class="form-control">
                    </div>
                    
            </div>
        </div>
        
        <div class="main-card mb-3 card">
            <div class="card-body">
                    
                    <div class="position-relative form-group">
                        <label for="" class="">Coordinates: </label>
                        <textarea name="coordinates" class="form-control"  
                        style="height: 200px;">
                        @if ($farmerprofile) 
                            @foreach($farmerprofile->farmCoordinates as $coordinates)
                            {{ $coordinates->lat }}, {{ $coordinates->lng }}
                            @endforeach
                        @else {{old('coordinates') }}
                        @endif
                        </textarea>
                        
                    </div>
                    
                        <div class="position-relative form-group">
                        <label for="" class="">Crop type: </label>
                        <input name="crop_type"  
                        type="text" class="form-control" 
                        @if ($farmerprofile) value="{{ $farmerprofile->crop_type }}"
                        @else value="{{old('crop_type') }}" 
                        @endif>
                    
                    <div class="position-relative form-group">
                        <label for="" class="">Land size: </label>
                        <input name="land"  
                        type="text" class="form-control" 
                        @if ($farmerprofile) value="{{ $farmerprofile->landSize }}"
                        @else value="{{old('land') }}" 
                        @endif>
                    </div>
                    
            </div>
        </div>
        @if ($farmerprofile)
	        <button class="mt-1 btn btn-primary">Update</button>
	    @else
	    	<button class="mt-1 btn btn-primary">Save</button>
	    @endif
    </div>
    
      </form>
</div>
@endsection