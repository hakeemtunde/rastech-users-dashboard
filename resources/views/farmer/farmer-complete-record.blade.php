<?php use Corebyte\RastechDashboard\Util\StorageUtil;?>
@extends('rastechdashboard::layout.admin')
@section('css')
<style>
    .tr-header {
        border-bottom: 1px solid #000;
    }
</style>
@endsection
 
@section('page-title')
<div class="page-title-subheading">
	<h3>Farmers complete record</h3>
</div>
@endsection @section('content')
<div class="row">
	<div class="col-md-12 col-xl-12">
		<div class="main-card mb-3 card">
        <div class="card-body">
        
            <table class="mb-0 table table-borderless">
                <tbody>
                @if($farmer->imageName != null)
                <?php 
                    $img_url = StorageUtil::getImageLocation($farmer, 'imageName');
                    
                ?>
                <tr>
                    <td></td>
                    <td></td>
                    <td><img alt="" src="{{$img_url}}"	width="110px" height="120px"></td>
                </tr>
                @endif
                <tr class="tr-header">
                    <td colspan="3">
                    	<h3>Farmer's detail</h3>
                    </td>
                </tr>
                
                <tr>
                    <td>First Name:  <span>{{$farmer->firstName}}</span></td>
                    <td>Middle Name: {{$farmer->middleName}}</td>
                    <td>Last Name: {{$farmer->sureName}}</td>
                </tr>
                <tr>
                    <td>State:  <span>{{$farmer->state}}</span></td>
                    <td>LGA:  <span>{{$farmer->lga}}</span></td>
                    <td>Gender: <span>{{$farmer->gender}}</span> </td>
                    
                </tr>
                <tr>
                    <td>BVN:  <span>{{$farmer->bvnNumber}}</span></td>
                    <td>Phone: <span>{{$farmer->phoneNumber}}</span></td>
                </tr>
                
                <tr>
                    <td colspan="3">Address:  <span>{{$farmer->homeAddress}}</span></td>
                </tr>
                
                <tr>
                    <td>Crop type:  <span>{{$farmer->crop_type}}</span></td>
                </tr>
                
                <tr class="tr-header">
                    <td colspan="3">
                    	<h3>Next of Kin</h3>
                    </td>
                </tr>
                @if($farmer->nextOfKin)
                <tr>
                    <td>First Name:  <span>{{$farmer->nextOfKin->firstName }}</span></td>
                    <td>Middle Name:  <span>{{$farmer->nextOfKin->middleName}}</span></td>
                    <td>Surname: <span>{{$farmer->nextOfKin->surename}}</span> </td>
                </tr>
                
                <tr>
                    <td>Relationship:  <span>{{ $farmer->nextOfKin->mobileNo }}</span></td>
                    <td>Phone:  <span>{{$farmer->nextOfKin->relationship}}</span></td>
                    <td></td>
                </tr>
                
                <tr>
                    <td colspan="3">Address:  <span>{{$farmer->nextOfKin->address }}</span></td>
                </tr>
                @endif
                <tr class="tr-header">
                    <td colspan="3">
                    	<h3>Farm coordinates</h3>
                    </td>
                </tr>
                
                <tr>
                    <td>Land size:  <span>{{$farmer->landSize}}</span></td>
                    <td>Lat:  <span>{{$farmer->latitude}}</span></td>
                    <td>Lng: <span>{{$farmer->longitude}}</span> </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
	</div>
</div>

<!-- Identification image -->
<div class="row">
	<div class="col-md-12 col-xl-12">
		<div class="main-card mb-3 card">
        <div class="card-body">
            <table class="mb-0 table table-borderless">
                <tbody>
                	 <tr class="tr-header">
                    <td colspan="3">
                    	<h3>Identification</h3>
                    </td>
                </tr>
                @if($farmer->identification != null)
                <tr>
                    <td>ID Type:  <span>{{$farmer->identification->idType}}</span> 
                	
                	@if($farmer->identificationFileName != null)
                	<?php 
                        $url_id = StorageUtil::getImageLocation($farmer, 'identificationFileName');
                    ?>
                    <a target="_blank" href="{{$url_id}}">View</a>
                    @endif
                    </td>
                    <td>Number/Serial:  <span>{{$farmer->identification->idNumber}}</span></td>
                    <td>Issue Date/Expire Date: 
                    @if($farmer->identification->issueDate != null) 
                    	<span>{{$farmer->identification->issueDate->format('d-m-Y')}}</span>
                    @endif 
                    @if($farmer->identification->expiryDate != null) 
                    / <span>  {{$farmer->identification->expiryDate->format('d-m-Y')}}</span>
                    @endif 
                    </td>
                </tr>
                @endif
                </tbody>
            </table>
                @if($farmer->identificationFileName != null)
            	<div>
            		<img alt=""  src="{{$url_id}}" width="560px" height="300px">
            	</div>
            	@endif
        </div>
    </div>
	</div>
</div>

<!-- Documents -->
<div class="row">
	<div class="col-md-12 col-xl-12">
		<div class="main-card mb-3 card">
        <div class="card-body">
            <table class="mb-0 table table-borderless">
                <tbody>
                	 <tr class="tr-header">
                    <td colspan="3">
                    	<h3>Farm documents</h3>
                    </td>
                </tr>
                </tbody>
            </table>
            <br>
            	
            	 @if($farmer->document1Name != null)
        	     <?php 
                 $url_document_1 = StorageUtil::getImageLocation($farmer, 'document1Name');
                 
                ?>
            	<div>
            		<img alt="" src="{{$url_document_1}}" width="560px" height="300px">
            	</div>
            	@endif
            	
            	@if($farmer->document2Name != null)
            	<?php 
                 $url_document_2 = StorageUtil::getImageLocation($farmer, 'document2Name');
                ?>
            	<div>
            		<h1>Farm documents</h1>
            		<img alt=""  src="{{$url_document_2}}" width="560px" height="300px">
            	</div>
            	@endif
       
        </div>
    </div>
	</div>
</div>
@endsection