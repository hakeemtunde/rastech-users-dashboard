@extends('rastechdashboard::layout.admin') @section('page-title')
<div class="page-title-subheading">
	<h3>{{ $title }}</h3>
</div>
@endsection @section('page-action')
<div class="d-inline-block dropdown">
	<button type="button" data-toggle="dropdown" aria-haspopup="true"
		aria-expanded="false" class="btn-shadow dropdown-toggle btn btn-info">
		<span class="btn-icon-wrapper pr-2 opacity-7"> <i
			class="fa fa-business-time fa-w-20"></i>
		</span> Download Record
	</button>
	<div tabindex="-1" role="menu" aria-hidden="true"
		class="dropdown-menu dropdown-menu-right">
		<ul class="nav flex-column">
			<li class="nav-item"><a class="nav-link"> <i
					class="nav-link-icon lnr-inbox"></i> <span> Spreadsheet</span>

			</a></li>
			<li class="nav-item"><a class="nav-link"> <i
					class="nav-link-icon lnr-book"></i> <span> PDF format</span>

			</a></li>
			<li class="nav-item"><a class="nav-link"> <i
					class="nav-link-icon lnr-picture"></i> <span> Word Document</span>
			</a></li>
		</ul>
	</div>
</div>
@endsection @section('content')
<div class="row">
	<div class="col-md-6 col-xl-4">
		<div class="card mb-3 widget-content bg-midnight-bloom">
			<div class="widget-content-wrapper text-white">
				<div class="widget-content-left">
					<div class="widget-heading">Total Uploads</div>
					<!-- 					<div class="widget-subheading">Today</div> -->
				</div>
				<div class="widget-content-right">
					<div class="widget-numbers text-white">
						<span>{{ $farmers->count() }}</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12 col-xl-12">
    	<div class="main-card mb-3 card">
    		<div class="card-body">
    			<table id="example" class="table table-hover table-striped table-bordered">
    				<thead>
    					<tr>
    						<th>S</th>
    						<!-- <th style="width: 200px;">Farmers ID</th> -->
    						<th>First Name</th>
    						<th>Middle Name</th>
    						<th>Surename</th>
    						<th>Gender</th>
							@if($type == 0)
    						<th>BVN</th>
    						<th>Coordinate</th>
    						<th>State</th>
    						<th>LGA</th>
    						<th>Farm size</th>
    						<th>Date</th>
    						<th style="width: 100px;">Action</th>
    						<th>Map</th>
							@endif
    						
    					</tr>
    				</thead>
    				<tbody>
    				<?php $couter = 0; ?>
    					@foreach($farmers as $famer)
    					<?php $couter++; ?>
    					<tr>
    						<td scope="row">{{ $couter }}</td>
    						<td>{{ $famer->firstName }}</td>
    						<td>{{ $famer->middleName }}</td>
    						<td>{{ $famer->sureName }}</td>
    						<td>{{ $famer->gender }}</td>
							@if($type == 0)
    						<td>{{ $famer->bvnNumber }}</td>
    						<td>Lat:{{ $famer->latitude }}, Lng: {{$famer->longitude}}</td>
    						<td>{{ $famer->state }}</td>
    						<td>{{ $famer->lga }}</td>
    						<td>{{ $famer->landSize }}</td>
    						<td>{{ $famer->created_at->format('m-d-Y') }}</td>
    						<td>
    							<div class="d-inline-block dropdown">
    								<button type="button" data-toggle="dropdown"
    									aria-haspopup="true" aria-expanded="false"
    									class="btn-shadow dropdown-toggle btn btn-info">
    									<span class="btn-icon-wrapper pr-2 opacity-7"> <i
    										class="fa fa-business-time fa-w-20"></i>
    									</span>
    								</button>
    								<div tabindex="-1" role="menu" aria-hidden="true"
    									class="dropdown-menu dropdown-menu-right">
    									<ul class="nav flex-column">
    										<li class="nav-item"><a href="{{ route('farmer.details', $famer) }}" class="nav-link"> <i
    												class="fa fa-people"></i> <span>View</span>
    										</a></li>
    										<li class="nav-item"><a href="{{ route('farmer.edit', $famer) }}" class="nav-link"> <i
    												class="fa fa-pencil"></i> <span>Edit</span>
    										</a></li>
    									</ul>
    								</div>
    							</div>
    						</td>
    						<td>
    							<a href="{{ route('farmer.map', $famer) }}"
    								class="btn mr-2 mb-2 btn-primary">
    								Map</button>
    						</td>
							@endif
    					</tr>
    					@endforeach
    
    			</table>
    		</div>
    	</div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" tabindex="-1" role="dialog"
	aria-labelledby="mySmallModalLabel" aria-hidden="true" id="mapModal">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Google Map</h5>
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div style="width: 700px; height: 600px;">
					<div id="map-canvas" style="width: 100%; height: 100%"></div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary">Save changes</button>
			</div>
		</div>
	</div>
</div>

@endsection
