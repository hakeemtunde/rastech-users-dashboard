<?php 
namespace Corebyte\RastechDashboard\Tests\Feature;

use Corebyte\RastechDashboard\Tests\TestCase;

class RouteTest extends TestCase 
{
    /** @test */ 
    public function can_reach_index_route()
    {
        $this->get(route('login'))
            ->assertSee('FMS Login');
    }
}