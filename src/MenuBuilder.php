<?php
namespace Corebyte\RastechDashboard;

use Corebyte\RastechDashboard\Consts\UserRoleConst;
use Corebyte\RastechDashboard\Consts\PathConst;
use Corebyte\RastechDashboard\Models\ClientGroupMenu;

class MenuBuilder
{
    private $user;

    public function __construct($user)
    {
        $this->user  = $user;
        $this->user->load('userRoles');
    }

    public function getMenu()
    {
        $menu = [];

        switch($this->getRole()) {
            case UserRoleConst::CLIENT_ROLE:
                $menu =  ClientGroupMenu::where('client_id',
                    $this->user->client->id)
                        ->with('groupSubMenuItem.menuItemGroups.subMenuItem')->get();
                break;

                case UserRoleConst::BUYER:
                    $menu = PathConst::getBuyerMenu();
                    break;
                case UserRoleConst::INPUT_PROVIDERS:

                    $menu = $this->buildServiceProvidersMenu();
                    break;
        }

        return $menu;
    }

    private function getRole()
    {
        if ($this->user->userRoles == null) return null;
        return $this->user->userRoles->role_name;
    }

    private function buildServiceProvidersMenu()
    {
        $menu = [];

        $operations = $this->user->clientServiceProviderUser
            ->clientServiceProvider->operations;

            // dd($operations);

        $operations->each(function($operation) use (&$menu){

            switch($operation->operation) {
                case UserRoleConst::INPUT_PROVIDERS:
                    $menu = PathConst::getInputServiceMenu();

                // case UserRoleConst::EQUIPMENTS:
                //     $menu['Equipments'] = PathConst::getEquipmentMenu();
                }
        });

        $menu['Equipments'] = PathConst::getEquipmentMenu();
        return $menu;
    }
}
