<?php
namespace Corebyte\RastechDashboard\Http\Composers;

use Illuminate\View\View;
use Illuminate\Http\Request;

use Corebyte\RastechDashboard\MenuBuilder;

class MenuComposer
{
    private $user;
    private $menuBuilder;
    
    public function __construct(Request $request)
    {
        $this->user = $user = $request->user();
       
        //refactor to use DI
        $this->menuBuilder = new MenuBuilder($user);    
    }

    public function compose(View $view)
    {
        $menus = $this->menuBuilder->getMenu();
        $view->with('menus', $menus)
            ->with('current_uri', url()->current())
            ->with('user', $this->user);
    }
}