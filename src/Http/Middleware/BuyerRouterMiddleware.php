<?php 
namespace Corebyte\RastechDashboard\Http\Middleware;

use Closure;
use Auth;
use Corebyte\RastechDashboard\Consts\UserRoleConst;

class BuyerRouterMiddleware 
{

    public function handle($request, $next)
    {
        if($request->user() == null) {
            Auth::logout();
            return redirect(route('login'));
        }

        $user = $request->user()->load('userRoles');
        if($user->userRoles == null || $user->userRoles->role_name != UserRoleConst::BUYER) {
            Auth::logout();
            return redirect(route('login'));
        }

        return $next($request);
    }
}