<?php

namespace Corebyte\RastechDashboard\Http\Controllers\ApiController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Corebyte\RastechDashboard\Http\Controllers\Controller;
use Corebyte\RastechDashboard\Services\ProductTransactionService;

class BuyerOrderController extends Controller
{
    public function placeOrder(Request $request)
    {
        $this->validate($request, [
            'client_id'=> 'required',
            'buyer_id'=> 'required',
            'company_id'=> 'required',
            'product_id'=> 'required',
            'service_type'=> 'required',
            'quantity'=> 'required',
        ]);

        $transaction = ProductTransactionService::addTransaction($request->all());
        return response($transaction->toArray(), 200);
    }
}


