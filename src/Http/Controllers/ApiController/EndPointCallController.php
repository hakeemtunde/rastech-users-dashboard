<?php

namespace Corebyte\RastechDashboard\Http\Controllers\ApiController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Corebyte\RastechDashboard\Http\Controllers\Controller;
use Corebyte\RastechDashboard\Models\ClientServiceProviderProduct;
use Corebyte\RastechDashboard\Models\ClientServiceProviderService;


class EndPointCallController extends Controller
{

    private const PRODUCT = 'products';
    private const SERVICES='services';

    public function fetchProductByType($companyid, $producttype)
    {

        if ($producttype == self::PRODUCT) {

            $products = ClientServiceProviderProduct::where('provider_id', $companyid)->get();
            return response($products, 200);
        }

        if ($producttype == self::SERVICES) {

            $products = ClientServiceProviderService::where('provider_id', $companyid)->get();
            return response($products, 200);
        }

        return response(['error'=>'error'], 422);
    }
}


