<?php

namespace Corebyte\RastechDashboard\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function index()
    {
        return view()->first(['rastechdashboard::login']);
    }

    public function authenticate(Request $request)
    {
        // dd('here');
        $request->validate([
            'username' => 'required|string',
            'password' => 'required'
        ]);

        $credentials = $request->only('username', 'password');
        
        if (auth('web')->attempt($credentials)) {

            //check users status
            if (!Auth::user()->status) {
                return back()->withErrors(['Account disabled!']);        
            }

            return redirect()->intended(route('dashboard'));
        }

        //redirect to login
        return back()->withErrors(['invalide credential!']);
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->to(route('login'));
    }
}


