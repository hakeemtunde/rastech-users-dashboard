<?php

namespace Corebyte\RastechDashboard\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Corebyte\RastechDashboard\Models\ClientServiceProvider;
use Corebyte\RastechDashboard\Models\ProductTransaction;

class BuyerOrderController extends Controller
{

    public function form(Request $request)
    {
        $client_id = $request->user()->clientBuyer->client_id;
        $user_id = $request->user()->id;
        $client_service_providers = 
            ClientServiceProvider::where('client_id', $client_id)->get();

        return view()
            ->first(['rastechdashboard::buyers.order.form'])
            ->with(compact('client_id'))
            ->with(compact('client_service_providers'))
            ->with(compact('user_id'));

    }

    //Buyer orders
    public function orders(Request $request) 
    {
        $user_id = $request->user()->id;
        $orders = ProductTransaction::where('status', 0)
            ->where('buyer_id', $user_id)->get();
        
        return view()
            ->first(['rastechdashboard::buyers.order.orders'])
            ->with(compact('orders'));
    }

    //company orders
    public function getCompanyOrders(Request $request) 
    {
        $provider_id = $request->user()->clientServiceProviderUser->client_service_provide_id;
        $orders = ProductTransaction::where('status', 0)
            ->where('company_id', $provider_id)->get();
        
        return view()
            ->first(['rastechdashboard::providers.clientprovider.product.order.orders'])
            ->with(compact('orders'));
    }

    public function getOrdersByProvider(Request $request, $provider_id) 
    {
        $orders = ProductTransaction::where('status', 0)
            ->where('company_id', $provider_id)->get();
        $provider = ClientServiceProvider::find($provider_id);
        return view()
            ->first(['rastechdashboard::providers.clientprovider.product.order.orders'])
            ->with(compact('orders'))
            ->with(compact('provider'));
    }

    public function deleteOrder($id)
    {
        $buyer->delete();
        return back();
    }
}


