<?php

namespace Corebyte\RastechDashboard\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Corebyte\RastechDashboard\Models\FarmersProfile;

class FarmerUploadController extends Controller
{
    
    public function farmerEnrolByWeb(Request $request)
    {
        $request->user()->load('client');
        $farmers = FarmersProfile::where('client_id', 
        $request->user()->client->id)
        ->where('reg_type', 1)->get();

        $title = 'Online registration record';        

        return view()->first(['rastechdashboard::farmer.farmers-record'])
            ->with(compact('farmers', 'title'))->with('type', 1);
    }

    public function geoMappedFarmers(Request $request)
    {
        $request->user()->load('client');
        $farmers = FarmersProfile::where('client_id', 
        $request->user()->client->id)
        ->where('reg_type', 0)->get();

        $title = 'Uplaoded farmers record';

        return view()->first(['rastechdashboard::farmer.farmers-record'])
            ->with(compact('farmers', 'title'))->with('type', 0);
    }

    public function show($id)
    {
        $farmer = FarmersProfile::findOrFail($id);
        $farmer->load('identification', 'nextOfKin');
        return view()->first(['rastechdashboard::farmer.farmer-complete-record'])->with(compact('farmer'));
    }

    public function edit($id)
    {
        $farmerprofile= FarmersProfile::findOrFail($id);
        $farmerprofile->load('farmCoordinates');
        return view()->first(['rastechdashboard::farmer.form'])
            ->with(compact('farmerprofile'));
    }

    public function openMap($id)
    {
        $farmers = FarmersProfile::findOrFail($id);
        $farmers->load('farmCoordinates');
        $coordinates = [];
        if ($farmers->farmCoordinates != null) {
            $coordinates = $farmers->farmCoordinates->map(function($fc){
                return [$fc->lat, $fc->lng];
            });
        }

        return view()->first(['rastechdashboard::farmer.map'])->with(compact('coordinates', 'farmers'));
    }


}


