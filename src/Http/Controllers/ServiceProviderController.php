<?php

namespace Corebyte\RastechDashboard\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Corebyte\RastechDashboard\Models\ClientServiceProvider;
use Corebyte\RastechDashboard\Models\User;
use Corebyte\RastechDashboard\Models\UserRole;
use Corebyte\RastechDashboard\Consts\UserRoleConst;
use Corebyte\RastechDashboard\Util\RandomString;
use Corebyte\RastechDashboard\Models\ClientServiceProviderUser;
use Hash;

class ServiceProviderController extends Controller
{
    public function show(Request $request)
    {
        $providers = ClientServiceProvider::where('client_id', 
            $request->user()->client->id)->get();
        return view()->first(['rastechdashboard::providers.index'])
            ->with(compact('providers'));
    }

    public function generateCredentials($id, Request $request)
    {
        $provider = ClientServiceProvider::findOrFail($id);

        $role = UserRole::where('role_name', 
            UserRoleConst::INPUT_PROVIDERS)->get();
        if($role->isEmpty()) {
            dd('INPUT_PROVIDERS role dont exists!, pls run migrations');
        }

        $gen_password = RandomString::generate_string();

        //create login credentials
        if ($provider->clientServiceProviderUser == null) {
            
            $user_data = [
                'username' => $provider->email,
                'role_id' => $role->first()->id, 
                'name' => $provider->name, 
                'password' => Hash::make($gen_password)
            ];

            $user_created = User::create($user_data);

            ClientServiceProviderUser::create([
                'client_service_provide_id' => $provider->id,
                'user_id' => $user_created->id,
                'gen_password' => $gen_password
            ]);

            $request->session()->flash('success', 
                'Credentials generated for'.$provider->name.' servive provider');

        } else {
            $provider->clientServiceProviderUser->user->update([
                'password' => Hash::make($gen_password)
            ]);

            $provider->clientServiceProviderUser
                ->update(['gen_password' => $gen_password]);

            $request->session()->flash('success', 
                'Credentials updated for '.$provider->name.' servive provider');
        }

        return back();
    }

    public function delete($id)
    {
        $provider = ClientServiceProvider::findOrFail($id);
        $provider->delete();
        return back();
    }
}


