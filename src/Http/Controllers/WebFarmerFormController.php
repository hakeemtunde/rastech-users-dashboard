<?php

namespace Corebyte\RastechDashboard\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Corebyte\RastechDashboard\Models\FarmersProfile;
use Corebyte\RastechDashboard\Models\FarmCoordinate;
use Corebyte\RastechDashboard\Consts\UserRoleConst;

class WebFarmerFormController extends Controller
{
    public function form()
    {
        $farmerprofile = null;
        return view()->first(['rastechdashboard::farmer.form'])
            ->with(compact('farmerprofile'));
    }

    public function save(Request $request) 
    {

        $rules = $this->prepareValidationRules($request, false);
        
        $this->validate($request, $rules);
        
        //prepare data
        $data = $request->except(['_token', 'picture', 'identification',
            'document1Name', 'document2Name']);
        
        $data['landSize'] = $request->input('land');
        $data['identificationFileName'] = '';
        $data['document1Name'] = '';
        $data['document2Name'] = '';
        $data['profileId'] = $request->user()->id;
        $data['user_id'] = $request->user()->id;
                
        $this->uploadAttachments($request, $data, false);
        
        $farmerprofile = FarmersProfile::create($data);
        
        $this->saveCoordinates($request, $farmerprofile);
        
        return redirect()->back()->with([
            'success' => 'Record saved!'
        ]);
    }
    
    public function show($farmerprofile)
    {
        $farmerprofile= FarmersProfile::find($farmerprofile);
        $farmerprofile->load('farmCoordinates');
        return view()->first(['rastechdashboard::farmer.form'])
            ->with(compact('farmerprofile'));
    }
    
    public function update(Request $request, $farmer)
    {
        $farmerprofile= FarmersProfile::find($farmer);
        
        $rules = $this->prepareValidationRules($request, true);
        //validate
        $this->validate($request, $rules);

        //prepare data
        $data = $request->except(['_token', 'picture', 'identification',
            'document1Name', 'document2Name']);
        
        $data['landSize'] = $request->input('land');
        $data['imageName'] = $farmerprofile->imageName;
        
        $this->uploadAttachments($request, $data, true);
        
        $this->saveCoordinates($request, $farmerprofile);
        
        $farmerprofile->update($data);
        
        return redirect()->back()->with([
            'success' => 'Record updated!'
        ])->with(compact('farmerprofile'));
                
        
    }
    
    public function delete($famerprofile)
    {
        $farmer = FarmersProfile::find($famerprofile);
        Storage::delete('farmers/'.$farmer->imageName);
        $farmer->delete();
        return back();
    }
    
    private function prepareValidationRules($request, $isUpdating)
    {
        $rules = [
            'firstName' => 'required',
            'state' => 'required',
            'lga' => 'required',
            'homeAddress' => 'required',
            'phoneNumber' => 'required',
            'gender' => 'required',
            'land' => 'required',
            'sureName' => 'required',
            'idType' => 'required'
        ];
        
        if (!$isUpdating) {
            $rules['bvnNumber'] =  'required|unique:farmers_profiles';
        } 
        
        if ($request->hasFile('picture')) {
            $rules['picture']  = 'required|mimes:jpeg,jpg';
        }
        
        return $rules;
        
    }
    
    private function uploadAttachments(Request $request, &$data, 
        $isUpdating) 
    {
        //store image
        if($request->hasFile('picture')) {
            
            if($isUpdating) {
                Storage::delete('public/farmers/'.$data['imageName']);
            }
            
            $path = $request->file('picture')->store('public/farmers');
            $filename = basename($path);
            $data['imageName'] = $filename;
        }
        
        
        if ($request->hasFile('identification')) {
            $path = $request->file('identification')->store('public/farmers');
            $filename = basename($path);
            $data['identification'] = $filename;
            
        }
        
        if ($request->hasFile('document1Name')) {
            $path = $request->file('document1Name')->store('public/farmers');
            $filename = basename($path);
            $data['document1Name'] = $filename;
            
        }
    }
    
    private function saveCoordinates($request, $farmerprofile)
    {
        $coordinate_text = $request->input('coordinates');
        if($coordinate_text == null) return;
        $lat_lngs = $this->extractCoordinates($coordinate_text);
        
        if ($lat_lngs == null) return;
        
        $this->deleteCoordinatesIfExists($farmerprofile);
        
        foreach ($lat_lngs as $lat_lng) {
            FarmCoordinate::create([
                'farmers_profile_id' => $farmerprofile->id,
                'profileId' => 0,
                'lat' => $lat_lng['lat'],
                'lng' => $lat_lng['lng'],
            ]);
        }
        
        if(count($lat_lngs) > 0) {
            $farmerprofile->update(
                ['latitude' => $lat_lngs[0]['lat'],
                    'longitude' => $lat_lngs[0]['lng']
                ]);
        }
    }
    
    private function extractCoordinates($coordinate_text) 
    {
        $pattern ='/[\d]{1,2}\.+\d*+\, -*+[\d]{1,2}+\.+\d*/';
        $match_counte = preg_match_all($pattern, $coordinate_text, $out);
        if ($match_counte == 0) return;
        $coordinates = [];
        
        //remove last item(map center coordinates)
        array_pop($out[0]);
        
        foreach($out[0] as $lat_lng) {
            $lat_lng_split = explode(',', $lat_lng);
            $coordinates[] = [
                'lat'=> $lat_lng_split[0], 
                'lng'=>$lat_lng_split[1]];
            
        }
        
        return $coordinates;
    }

    public function registeredfarmers(Request $request)
    {
        $farmers = FarmersProfile::all();
        //Web farmer 
        if ($request->user()->userRoles->role_name == UserRoleConst::WEB_ENROLLER) {
            $farmers = FarmersProfile::where('user_id', $request->user()->id)->get();
        }
        $title = 'Online registration record';        
        return view()->first(['rastechdashboard::farmer.farmers-record'])
        ->with(compact('farmers', 'title'))->with('type', 0);
    }
    
    private function deleteCoordinatesIfExists($farmerprofile)
    {
        $farmerprofile->load('farmCoordinates');
        
        if ($farmerprofile->farmCoordinates->isEmpty()) return;
        
        FarmCoordinate::where('farmers_profile_id', 
            $farmerprofile->id)->delete();
        
    }
}
