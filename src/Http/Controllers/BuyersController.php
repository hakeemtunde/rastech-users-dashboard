<?php

namespace Corebyte\RastechDashboard\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Corebyte\RastechDashboard\Models\ClientBuyer;
use Corebyte\RastechDashboard\Models\User;

class BuyersController extends Controller
{
    public function show(Request $request)
    {
        $buyers = ClientBuyer::where('client_id', 
            $request->user()->client->id)->get();
        return view()->first(['rastechdashboard::buyers.index'])
            ->with(compact('buyers'));
    }

    public function delete($id)
    {
        $buyer = ClientBuyer::findOrFail($id);
        User::find($buyer->user_id)->delete();
        return back();
    }
}


