<?php

namespace App\Http\Controllers\Provider;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\LocalMarketPrice;
use Carbon\Carbon;

class LocalMarketPriceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $local_market_prices = LocalMarketPrice::all();
        return view('provider.localmarketprice.index')
            ->with(compact('local_market_prices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $local_market_price = null; 
        return view('provider.localmarketprice.form')
            ->with(compact('local_market_price'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'price' => 'required',
            'unit' => 'required'
        ]);
        
        $data = $request->except('_token');
        
        $data['current_date'] = Carbon::now()->format('Y-m-d');
        $data['company_profile_id'] = $request->user()->companyProfile->id;
        
        LocalMarketPrice::create($data);
        
        return back()->with(['success'=>'Market item added']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(LocalMarketPrice $localmarketprice)
    {
        $local_market_price = $localmarketprice;
        return view('provider.localmarketprice.form')
        ->with(compact('local_market_price'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LocalMarketPrice $localmarketprice)
    {
        $request->validate([
            'name' => 'required',
            'price' => 'required',
            'unit' => 'required'
        ]);
        
        $data = $request->except('_token');
        
        $data['prev_date'] = Carbon::createFromFormat('d-m-Y', $localmarketprice->current_date)->format('Y-m-d');
        $data['current_date'] = Carbon::now()->format('Y-m-d');
        
        $localmarketprice->update($data);
        return back()->with(['success'=>'Market item updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }
}
