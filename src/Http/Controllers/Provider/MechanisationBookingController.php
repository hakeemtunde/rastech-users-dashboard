<?php

namespace App\Http\Controllers\Provider;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Mechanisation;
use App\Models\MechanisationBooking;

class MechanisationBookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $company_profile_id = $request->user()->companyProfile->id;
        $mechanisationBookings = MechanisationBooking::where('company_profile_id', $company_profile_id)->get();
        $mechanisationBookings->load('mechanisation');
        return view('provider.mechanisations.booking')->with(compact('mechanisationBookings'));
    }
}
