<?php

namespace Corebyte\RastechDashboard\Http\Controllers\Provider;

use Illuminate\Http\Request;
use Corebyte\RastechDashboard\Http\Controllers\Controller;
use Corebyte\RastechDashboard\Models\ClientServiceProviderMechanisation as Mechanisation;

class MechanisationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mechanisations = Mechanisation::all();
        return view()->first(['rastechdashboard::providers.clientprovider.mechanisations.index'])
            ->with(compact('mechanisations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function form()
    {
        $mech =null;
        return view()->first(['rastechdashboard::providers.clientprovider.mechanisations.form'])
        ->with(compact('mech'));;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required', 
            'unit' => 'required',
            'cost' => 'required'
        ]);
        
        $provider_id = $request->user()
                                ->clientServiceProviderUser
                                ->client_service_provide_id;
        
        $data = $request->except('_token');
        $data['provider_id'] = $provider_id;
        
        Mechanisation::create($data);

        $request->session()->flash('success', 'service added successfully');

        return redirect()->back()
            ->with(['success'=> 'service added successfully']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mech = Mechanisation::findOrFail($id);
        return view()->first(['rastechdashboard::providers.clientprovider.mechanisations.form'])
            ->with(compact('mech'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'unit' => 'required',
            'cost' => 'required'
        ]);
        
        $data = $request->except('_token');
       
        Mechanisation::findOrFail($id)->update($data);
        
        $request->session()->flash('success', 'service updated successfully');

        return redirect()->back()
            ->with(['success'=> 'service updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id, Request $request)
    {
       Mechanisation::findOrFail($id)->delete();
       $request->session()->flash('success', 'Deleted successfully');
       return redirect()->back();
    }
    
}
