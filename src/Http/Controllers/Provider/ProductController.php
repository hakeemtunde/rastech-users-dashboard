<?php

namespace Corebyte\RastechDashboard\Http\Controllers\Provider;

use Illuminate\Http\Request;
use Corebyte\RastechDashboard\Http\Controllers\Controller;
use Corebyte\RastechDashboard\Consts\ProductConst;
use Corebyte\RastechDashboard\Models\ClientServiceProviderProduct as Product;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        $products = Product::where('provider_id', 
            $request->user()->clientServiceProviderUser
                ->client_service_provide_id)->get();
        return view()->first(['rastechdashboard::providers.clientprovider.product.index'])
            ->with(compact('products'));
    }
    
    public function store(Request $request)
    {
        $request->validate([
            ProductConst::TYPE => 'required|string',
            ProductConst::NAME => 'required',
            ProductConst::VARIETY => 'required',
            ProductConst::MEASURE_UNIT => 'required',
            ProductConst::UNIT_PRICE => 'required|integer'
        ]);
        
        $data = $request->except('_token');
        $data['provider_id'] = $request->user()
            ->clientServiceProviderUser
            ->client_service_provide_id;

        Product::create($data);
        
        return back()->with('success', 'Product added');   
    }

    public function form()
    {
        return view()
            ->first(['rastechdashboard::providers.clientprovider.product.form']);
    }

    public function edit($id)
    {
        $product = Product::findOrFail($id);
        return view()->first(['rastechdashboard::providers.clientprovider.product.edit-product'])
            ->with(compact('product'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            ProductConst::TYPE => 'required|string',
            ProductConst::NAME => 'required',
            ProductConst::VARIETY => 'required',
            ProductConst::MEASURE_UNIT => 'required',
            ProductConst::UNIT_PRICE => 'required|integer'
        ]);
        
        $product = Product::findOrFail($id)
            ->update($request->except('_token'));
        
        return back()->with('success', 'Product updated');   

    }

    public function delete($id, Request $request)
    {
        $product = Product::findOrFail($id)->delete();
        $request->session()->flash('success', 'Product deleted');
        return back();   
    }
}
