<?php

namespace App\Http\Controllers\Provider;

use Illuminate\Http\Request;
use App\Models\PurchaseBooking;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class PurchaseBookingController extends Controller
{
    public function index() 
    {
        $company_profile_id = Auth::user()->companyProfile->id;
        $purchasebookings = PurchaseBooking::where('company_profile_id', 
            $company_profile_id)
            ->get()->load('product');
        
       return view('provider.booking')->with(compact('purchasebookings'));
    }
}
