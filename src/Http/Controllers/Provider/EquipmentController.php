<?php

namespace Corebyte\RastechDashboard\Http\Controllers\Provider;

use Illuminate\Http\Request;
use Corebyte\RastechDashboard\Http\Controllers\Controller;
use Corebyte\RastechDashboard\Models\Equipment;

class EquipmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $equipments = Equipment::all();
        return view()->first(['rastechdashboard::providers.clientprovider.equipment.index'])
            ->with(compact('equipments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function form()
    {
        $equipment =null;
        return view()->first(['rastechdashboard::providers.clientprovider.equipment.form'])
        ->with(compact('equipment'));;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate($this->formValidationRules());
        
        $provider_id = $request->user()
                                ->clientServiceProviderUser
                                ->client_service_provide_id;

        $data = $request->except('_token');
        $data['provider_id'] = $provider_id;
        
        Equipment::create($data);

        $request->session()->flash('success', 'Equipment added successfully');

        return redirect()->back()
            ->with(['success'=> 'Equipment added successfully']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $equipment = Equipment::findOrFail($id);
        return view()->first(['rastechdashboard::providers.clientprovider.equipment.form'])
            ->with(compact('equipment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate($this->formValidationRules());
        
        $data = $request->except('_token');
       
        Equipment::findOrFail($id)->update($data);
        
        $request->session()->flash('success', 'Equipment updated successfully');

        return redirect()->back()
            ->with(['success'=> 'Equipment updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id, Request $request)
    {
       Equipment::findOrFail($id)->delete();
       $request->session()->flash('success', 'Deleted successfully');
       return redirect()->back();
    }

    private function formValidationRules()
    {
        return [
            'name' => 'required', 
            'type' => 'required',
            'model' => 'required',
            'capacity' => 'required',
            'cost' => 'required'
        ];
    }
    
}
