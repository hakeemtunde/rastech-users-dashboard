<?php 
namespace Corebyte\RastechDashboard;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Route;
use Illuminate\Routing\Router;
use Corebyte\RastechDashboard\MenuBuilder;

use Corebyte\RastechDashboard\Http\Middleware\AdminRouterMiddleware;
use Corebyte\RastechDashboard\Http\Middleware\BuyerRouterMiddleware;
use Corebyte\RastechDashboard\Http\Middleware\ClientServiceProviderRouterMiddleware;

class AppServiceProvider extends ServiceProvider 
{

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // $this->app->bind('Corebyte\RastechDashboard\Http\Composers\MenuComposer', 
        //     function ($app)
        //     {
        //         return new MenuComposer(
        //                 $app->make('Request'));
        //     });
        $this->mergeConfigFrom(__DIR__.'/../config/config.php', 'rastechdashboard');
    }

     /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //routers register middleware
        $router = $this->app->make(Router::class);
        $router->aliasMiddleware('admin-middleware', AdminRouterMiddleware::class);
        $router->aliasMiddleware('buyer-middleware', BuyerRouterMiddleware::class);
        $router->aliasMiddleware('provider-middleware', ClientServiceProviderRouterMiddleware::class);
        
        //register routes
        $this->registerRoutes();

        //register views
        $this->loadViewsFrom(__DIR__.'/../resources/views', 
            'rastechdashboard');

        //view publish
        if($this->app->runningInConsole()) {
            
            //publish views
            $this->publishes([
                __DIR__.'/../resources/views' => resource_path('views/vendor/rastechdashboard')
            ], 'views');

            // publish assets
            $this->publishes([
                __DIR__.'/../resources/assets' => public_path('rastechdashboard'),
                ], 'assets');
        }

    }

    protected function registerRoutes()
    {
        Route::group($this->routeSecureConfiguration(), function () {
            $this->loadRoutesFrom(__DIR__.'/../routes/web.php');
        });

        Route::group($this->routeUnsecureConfiguration(), function () {
            $this->loadRoutesFrom(__DIR__.'/../routes/web-unsecure.php');
        });

        Route::group($this->routeAdminSecureConfiguration(), function () {
            $this->loadRoutesFrom(__DIR__.'/../routes/admin-route.php');
        });

        Route::group($this->routeBuyerConfig(), function () {
            $this->loadRoutesFrom(__DIR__.'/../routes/buyer-route.php');
        });

        Route::group($this->routeClientServiceProviderConfig(), function () {
            $this->loadRoutesFrom(__DIR__.'/../routes/client-service-provider-route.php');
        });

        Route::group($this->apiRouteConfig(), function () {
            $this->loadRoutesFrom(__DIR__.'/../routes/api.php');
        });
    }

    protected function routeSecureConfiguration() 
    {
        return [
            'prefix'=> config('rastechdashboard.prefix'),
            'middleware' => config('rastechdashboard.middleware-secured')
        ];
    }

    protected function routeUnsecureConfiguration()
    {
        return [
            'prefix'=> config('rastechdashboard.prefix'),
            'middleware' => config('rastechdashboard.middleware-unsecured')
        ];
    }

    protected function routeAdminSecureConfiguration()
    {
        return [
            'prefix'=> config('rastechdashboard.prefix'),
            'middleware' => config('rastechdashboard.middleware-admin')
        ];
    }

    protected function routeBuyerConfig()
    {
        return [
            'prefix'=> config('rastechdashboard.prefix'),
            'middleware' => config('rastechdashboard.middleware-buyer')
        ];
    }

    protected function routeClientServiceProviderConfig()
    {
        return [
            'prefix'=> config('rastechdashboard.prefix'),
            'middleware' => config('rastechdashboard.middleware-provider')
        ];
    }

    protected function apiRouteConfig()
    {
        return [
            'prefix'=> config('rastechdashboard.prefix-api'),
            'middleware' => config('rastechdashboard.middleware-api')
        ];
    }
}