<?php

namespace Corebyte\RastechDashboard\Models;

use Illuminate\Database\Eloquent\Model;

class BuyerOrder extends Model
{
    protected $fillable = [
        'client_service_provider_id',
        'client_buyer_id',
        'product_id',
        'quantity',
        'unit_cost',
        'total_cost',
    ];
}
