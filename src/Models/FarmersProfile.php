<?php
namespace Corebyte\RastechDashboard\Models;

use Illuminate\Database\Eloquent\Model;

class FarmersProfile extends Model
{
    protected $fillable = [
        'user_id',
        'profileId',
        'upload_batch_no',
        'farmerId',
        'firstName',
        'state',
        'lga',
        'homeAddress',
        // 'workAddress',
        'bvnNumber',
        'phoneNumber',
        'status',
        'gender',
        'imageName',
        'identificationFileName',
        'latitude',
        'longitude',
        'landSize',
        'middleName',
        'sureName',
        'document1Name',
        'document2Name',
        'crop_type',
        'client_id',
        'farmer_gen_id'
    ];

    public function identification()
    {
        return $this->hasOne(Identification::class);
    }

    public function nextOfKin()
    {
        return $this->hasOne(NextOfKin::class);
    }

    public function farmCoordinate()
    {
        return $this->hasOne(FarmCoordinate::class);
    }

    public function farmCoordinates()
    {
        return $this->hasMany(FarmCoordinate::class);
    }
}
