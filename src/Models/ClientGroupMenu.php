<?php

namespace Corebyte\RastechDashboard\Models;

use Illuminate\Database\Eloquent\Model;

class ClientGroupMenu extends Model
{
    protected $fillable = [
        'group_sub_menu_item_id',
        'client_id'
    ];
    
    public function client()
    {
        return $this->belongsTo(Client::class);
    }
    
    public function groupSubMenuItem()
    {
        return $this->belongsTo(GroupSubMenuItem::class, 
            'group_sub_menu_item_id');
    }
}
