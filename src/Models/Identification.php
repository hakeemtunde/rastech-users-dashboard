<?php

namespace Corebyte\RastechDashboard\Models;

use Illuminate\Database\Eloquent\Model;

class Identification extends Model
{
    protected $fillable = [
        'farmers_profile_id',
        'profileId',
        'idType',
        'idNumber',
        'issueDate',
        'expiryDate',
        'identificationFileName'
    ];
    
    protected $dates = ['issueDate',
        'expiryDate'];
    //
    
    public function farmersProfile()
    {
        return $this->belongsTo(FarmersProfile::class);
    }
}
