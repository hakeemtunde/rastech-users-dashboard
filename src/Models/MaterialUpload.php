<?php

namespace Corebyte\RastechDashboard\Models;

use Illuminate\Database\Eloquent\Model;

class MaterialUpload extends Model
{
    protected $fillable = [
        'user_id',
        'title',
        'description',
        'material_type',
        'status',
        'filename',
        'url',
        'file_ext', 'category'
    ];
}
