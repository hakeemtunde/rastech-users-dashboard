<?php

namespace Corebyte\RastechDashboard\Models;

use Illuminate\Database\Eloquent\Model;

class ClientServiceProviderUser extends Model
{
    protected $fillable = [
        'client_service_provide_id',
        'user_id',
        'gen_password'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function clientServiceProvider()
    {
        return $this->belongsTo(ClientServiceProvider::class, 'client_service_provide_id');
    }
}
