<?php

namespace Corebyte\RastechDashboard\Models;

use Illuminate\Database\Eloquent\Model;

class FarmCoordinate extends Model
{
    protected $fillable = ['farmers_profile_id','profileId', 'lat', 'lng'];
}
