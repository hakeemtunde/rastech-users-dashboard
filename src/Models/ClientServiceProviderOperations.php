<?php

namespace Corebyte\RastechDashboard\Models;

use Illuminate\Database\Eloquent\Model;

class ClientServiceProviderOperations extends Model
{
    protected $fillable = ['provider_id',
        'operation'
    ];
}
