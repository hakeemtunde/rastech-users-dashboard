<?php

namespace Corebyte\RastechDashboard\Models;

use Illuminate\Database\Eloquent\Model;

class GroupSubMenuItem extends Model
{
    protected $fillable = [
        'group_heading',
        'name',
        'icon',
        'status'
        ];
    
    public function menuItemGroups()
    {
        return $this->hasMany(MenuItemGroup::class);
    }
}
