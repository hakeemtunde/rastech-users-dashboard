<?php

namespace Corebyte\RastechDashboard\Models;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = ['name', 'project', 
        'slug', 'address', 'phone', 'email', 'users_id'];
}
