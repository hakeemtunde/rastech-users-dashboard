<?php

namespace Corebyte\RastechDashboard\Models;

use Illuminate\Database\Eloquent\Model;

class ClientServiceProvider extends Model
{
    protected $fillable = ['name', 'email','address', 
        'phone', 
    'status', 'client_id'];

    public function operations()
    {
        return $this->hasMany(
            ClientServiceProviderOperations::class,  
            'provider_id');
    }

    public function clientServiceProviderUser()
    {
        return $this->hasOne(ClientServiceProviderUser::class, 'client_service_provide_id');
    }
    
    
}
