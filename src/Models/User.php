<?php

namespace Corebyte\RastechDashboard\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;


class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'password', 'role_id', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
   
    public function userRoles() 
    {
        return $this->belongsTo(UserRole::class, 'role_id');    
    }
    
    public function client()
    {
        return $this->hasOne(Client::class, 'users_id');
    }

    public function clientBuyer()
    {
        return $this->hasOne(ClientBuyer::class);
    }

    public function clientServiceProviderUser()
    {
        return $this->hasOne(
            ClientServiceProviderUser::class);
    }
    
    public function getStatus()
    {
        return $this->attributes['status'] ? 'Enabled' : 'Disabled';
    }
    
}
