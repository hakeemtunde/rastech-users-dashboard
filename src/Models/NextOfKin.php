<?php

namespace Corebyte\RastechDashboard\Models;

use Illuminate\Database\Eloquent\Model;

class NextOfKin extends Model
{
    protected $fillable = [
        'farmers_profile_id',
        'profileId',
        'firstName',
        'middleName',
        'surename',
        'relationship',
        'mobileNo',
        'address',
    ];
}
