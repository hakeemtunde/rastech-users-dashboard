<?php

namespace Corebyte\RastechDashboard\Models;

use Illuminate\Database\Eloquent\Model;

class SubMenuItem extends Model
{
    protected $fillable = [
        'name',
        'tag',
        'route',
        'icon',
        'uri',
        'status'
    ];   
}
