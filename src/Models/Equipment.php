<?php

namespace Corebyte\RastechDashboard\Models;

use Illuminate\Database\Eloquent\Model;

class Equipment extends Model
{
    protected $fillable = [
        'provider_id',
        'type', 'name', 'model', 'capacity',
        'cost','description', 'status'
    ];
}
