<?php

namespace Corebyte\RastechDashboard\Models;

use Illuminate\Database\Eloquent\Model;

class ClientBuyer extends Model
{
    protected $fillable =[
        'firstName',
        'middleName',
        'sureName',
        'phoneNumber',
        'email',
        'address',
        'gender',
        'user_id',
        'client_id'
    ];

    public function getFullName() 
    {
        return strtoupper($this->firstName . ' '. $this->middleName . ' '. $this->sureName);
    }
    
    
}
