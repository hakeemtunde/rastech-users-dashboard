<?php

namespace Corebyte\RastechDashboard\Models;

use Illuminate\Database\Eloquent\Model;

class ClientServiceProviderMechanisation extends Model
{
    protected $fillable =['provider_id',
        'name', 'unit', 'cost', 'status'];
}
