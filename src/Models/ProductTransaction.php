<?php

namespace Corebyte\RastechDashboard\Models;

use Illuminate\Database\Eloquent\Model;

class ProductTransaction extends Model
{
    protected $fillable = [
       'buyer_id','client_id', 'company_id', 'product_id',
       'quantity', 'price','total', 'order_id'
    ];

    protected $dates = [
        'created_at', 'updated_at'
    ];

    public function generateAndUpdateOrderId()
    {
        $order_id = $this->id.$this->created_at->format('Y').$this->product_id;
        $this->update(['order_id'=> $order_id]);
    }

    public function serviceProvider()
    {
        return $this->belongsTo(ClientServiceProvider::class, 'company_id');
    }

    public function product()
    {
        return $this->belongsTo(ClientServiceProviderProduct::class, 'product_id');
    }

    public function clientBuyer()
    {
        return $this->belongsTo(ClientBuyer::class, 'buyer_id', 'user_id');
    }
}
