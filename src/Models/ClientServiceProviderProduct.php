<?php

namespace Corebyte\RastechDashboard\Models;

use Corebyte\RastechDashboard\Consts\ProductConst;
use Illuminate\Database\Eloquent\Model;

class ClientServiceProviderProduct extends Model
{
    protected $fillable = [
        ProductConst::NAME,
        ProductConst::TYPE,
        ProductConst::VARIETY,
        ProductConst::UNIT_PRICE,
        ProductConst::LOCATIONS,
        ProductConst::MEASURE_UNIT,
        'provider_id'
    ];

    public function clienServiceProvider()
    {
        return $this->belongsTo(
            ClientServiceProvider::class, 
            'provider_id');
    }
}
