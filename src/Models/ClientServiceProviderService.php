<?php

namespace Corebyte\RastechDashboard\Models;

use Illuminate\Database\Eloquent\Model;

class ClientServiceProviderService extends Model
{
    protected $fillable =['provider_id',
    'name', 'unit', 'cost', 'description','status'];
}
