<?php

namespace Corebyte\RastechDashboard\Models;

use Illuminate\Database\Eloquent\Model;
use Corebyte\RastechDashboard\Consts\ProductConst;

class Product extends Model
{
    protected $fillable = [
        ProductConst::NAME,
        ProductConst::TYPE,
        ProductConst::VARIETY,
        ProductConst::UNIT_PRICE,
        ProductConst::LOCATIONS,
        ProductConst::MEASURE_UNIT,
        'company_profile_id'
    ];

    //model use buy both company and client service provider
    public function clientServiceProvider()
    {
        return $this->belongsTo(ClientServiceProvider::class, 
            'company_profile_id');
    }
    
}
