<?php

namespace Corebyte\RastechDashboard\Models;

use Illuminate\Database\Eloquent\Model;

class MenuItemGroup extends Model
{
    protected $fillable = [
        'group_sub_menu_item_id',
        'sub_menu_item_id'
    ];
    
    public function groupSubMenuItem()
    {
        return $this->belongsTo(GroupSubMenuItem::class);
    }
    
    public function subMenuItem()
    {
        return $this->belongsTo(SubMenuItem::class);
    }
}
