<?php

namespace Corebyte\RastechDashboard\Consts;

class PurchaseBookingConst
{
    const FARMER_ID='farmer_id', 
           COMPNAY_PROFILE_ID='company_profile_id',
           PRODUCT_ID='product_id',
            QUANTITY='quantity',
           UNIT_COST='unit_cost',
           TOTAL_COST='total_cost',
           FULL_NAME='full_name',
           PHONE='phone',
           ADDRESS='address';
}

