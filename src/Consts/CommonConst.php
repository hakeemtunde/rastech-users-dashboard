<?php

namespace Corebyte\RastechDashboard\Consts;

class CommonConst
{

    const STATUS_PENDING = 'PENDING', 
        STATUS_REFRESH = 'REFRESH',
        STATUS_DELETED = 'DELETED',
        ACTION_NEW = 'NEW', 
        ACTION_DELETED = 'DELETED', 
        ACTION_UPDATED='UPDATED';
}

