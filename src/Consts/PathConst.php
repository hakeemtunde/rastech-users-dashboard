<?php 
namespace Corebyte\RastechDashboard\Consts;

class PathConst 
{
    public static function getBuyerMenu()
    {
        return [
            'Buyer' => [
                'Resources/Services' => [
                    'Place order' => route('buyer.order.form'),
                    'View orders' => route('buyer.orders.all'),
                    'Transactions' => route('buyer.transaction.all')
                ]
            ]
        ];
    }

    public static function getInputServiceMenu()
    {
        return  [
            'Products/Service' => [
                'Company products' => [
                    'Add new' => route('service.provider.products.form'),
                    'Products' => route('service.provider.products.index'),
                ]
            ],
            'Bookings' => [
                    'Service/Product booking' => [
                        'Buyers orders' => route('service.provider.product.orders'),
                        'Service bookings' => null
                    ]
                ],
            'Activity Logs' => [
                'Trasactions' => [
                    'View transactions' => null
                ]
            ]
        ];
    }

    public static function getMechanizationMenu()
    {
        return 
             [
                'Mechanization' => [
                    'Create' => route('service.provider.mech.form'),
                    'Services' => route('service.provider.mech.index')
                ]
            ];
    }

    public static function getEquipmentMenu()
    {
        return 
             [
                'Equipments' => [
                    'Add new' => route('service.provider.equipments.form'),
                    'View equipments' => route('service.provider.equipments.index')
                ]
            ];
    }
}