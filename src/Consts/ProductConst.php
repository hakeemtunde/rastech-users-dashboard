<?php

namespace Corebyte\RastechDashboard\Consts;

class ProductConst
{
    const NAME='name', TYPE='type', VARIETY='variety', MEASURE_UNIT='measure_unit',
    UNIT_PRICE='unit_price', LOCATIONS='locations';
}

