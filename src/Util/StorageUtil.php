<?php 
namespace Corebyte\RastechDashboard\Util;

use Illuminate\Support\Facades\Storage;

class StorageUtil 
{
    public static function getImageLocation($farmer, $docName, $folder='farmers') 
    {
        return Storage::url("{$folder}/".$farmer->{$docName});
    }
}