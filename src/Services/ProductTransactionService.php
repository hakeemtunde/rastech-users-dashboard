<?php

namespace Corebyte\RastechDashboard\Services;

use Corebyte\RastechDashboard\Models\ProductTransaction;
use Corebyte\RastechDashboard\Models\ClientServiceProviderProduct;
class ProductTransactionService 
{


    public static function addTransaction($data) 
    {
        $orderProduct = ClientServiceProviderProduct::findOrFail($data['product_id']);
        $data['price'] = $orderProduct->unit_price;
        $data['total'] = $orderProduct->unit_price * $data['quantity'];
        $data['company_id'] = $orderProduct->provider_id;
        $data['order_id'] = 0000000;
        $transaction = ProductTransaction::create($data);
        $transaction->generateAndUpdateOrderId();
        return $transaction;
    }


}