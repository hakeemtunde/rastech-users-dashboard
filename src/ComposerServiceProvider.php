<?php
namespace Corebyte\RastechDashboard;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use Auth;
class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

     /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        View::composer(
            'rastechdashboard::partials.sidebar', 
                'Corebyte\RastechDashboard\Http\Composers\MenuComposer'
        );

        view()->share('version', '0.0.1');
    }
}