<?php 
return [
    'prefix' => 'agroservice',
    'prefix-api' => 'api',
    'middleware-secured' => ['web', 'auth'], // you probably want to include 'web' here
    'middleware-unsecured' => ['web'],
    'middleware-admin' => ['web', 'auth', 'admin-middleware'],
    'middleware-buyer' => ['web', 'auth', 'buyer-middleware'],
    'middleware-provider' => ['web', 'auth', 'provider-middleware'],

    'middleware-api' => ['api'],
];
